/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "ans_distributed_proxy.h"
#include "ans_const_define.h"
#include "ans_inner_errors.h"
#include "ans_log_wrapper.h"
#include "message_option.h"
#include "message_parcel.h"
#include "parcel.h"


namespace OHOS::Notification {

ErrCode AnsDistributedProxy::OnDistributedKvStoreDeathRecipient()
{
    ANS_LOGD("%{public}s", __FUNCTION__);
    MessageParcel data;
    if (!data.WriteInterfaceToken(AnsDistributedProxy::GetDescriptor())) {
        ANS_LOGE("[OnDistributedKvStoreDeathRecipient] fail: write interface token failed.");
        return ERR_ANS_PARCELABLE_FAILED;
    }

    MessageParcel reply;
    MessageOption option = {MessageOption::TF_SYNC};
    ErrCode result = InnerTransact(AnsDistributedInterfaceCode::DISTRIBUTEDKVSTOREDEATHRECIPIENT, option, data, reply);
    if (result != ERR_OK) {
        ANS_LOGE("[OnDistributedKvStoreDeathRecipient] fail: transact ErrCode=%{public}d", result);
        return ERR_ANS_TRANSACT_FAILED;
    }

    if (!reply.ReadInt32(result)) {
        ANS_LOGE("[OnDistributedKvStoreDeathRecipient] fail: read result failed.");
        return ERR_ANS_PARCELABLE_FAILED;
    }

    return result;
}

ErrCode AnsDistributedProxy::Delete(const std::string &bundleName, const std::string &label, int32_t id)
{
    ANS_LOGD("%{public}s", __FUNCTION__);
    MessageParcel data;
    if (!data.WriteInterfaceToken(AnsDistributedProxy::GetDescriptor())) {
        ANS_LOGE("[Delete] fail: write interface token failed.");
        return ERR_ANS_PARCELABLE_FAILED;
    }

    if (!data.WriteString(bundleName)) {
        ANS_LOGE("[Delete] fail: write bundleName failed");
        return ERR_ANS_PARCELABLE_FAILED;
    }

    if (!data.WriteString(label)) {
        ANS_LOGE("[Delete] fail: write label failed");
        return ERR_ANS_PARCELABLE_FAILED;
    }

    if (!data.WriteInt32(id)) {
        ANS_LOGE("[Delete] fail: write id failed");
        return ERR_ANS_PARCELABLE_FAILED;
    }

    MessageParcel reply;
    MessageOption option = {MessageOption::TF_SYNC};
    ErrCode result = InnerTransact(AnsDistributedInterfaceCode::DISTRIBUTEDDELETE, option, data, reply);
    if (result != ERR_OK) {
        ANS_LOGE("[Delete] fail: transact ErrCode=%{public}d", result);
        return ERR_ANS_TRANSACT_FAILED;
    }

    if (!reply.ReadInt32(result)) {
        ANS_LOGE("[Delete] fail: read result failed.");
        return ERR_ANS_PARCELABLE_FAILED;
    }

    return result;
}

ErrCode AnsDistributedProxy::DeleteRemoteNotification(const std::string &deviceId,
    const std::string &bundleName, const std::string &label, int32_t id)
{
    ANS_LOGD("%{public}s", __FUNCTION__);
    MessageParcel data;
    if (!data.WriteInterfaceToken(AnsDistributedProxy::GetDescriptor())) {
        ANS_LOGE("[DeleteRemoteNotification] fail: write interface token failed.");
        return ERR_ANS_PARCELABLE_FAILED;
    }

    if (!data.WriteString(deviceId)) {
        ANS_LOGE("[DeleteRemoteNotification] fail: write deviceId failed");
        return ERR_ANS_PARCELABLE_FAILED;
    }

    if (!data.WriteString(bundleName)) {
        ANS_LOGE("[DeleteRemoteNotification] fail: write bundleName failed");
        return ERR_ANS_PARCELABLE_FAILED;
    }

    if (!data.WriteString(label)) {
        ANS_LOGE("[DeleteRemoteNotification] fail: write label failed");
        return ERR_ANS_PARCELABLE_FAILED;
    }

    if (!data.WriteInt32(id)) {
        ANS_LOGE("[DeleteRemoteNotification] fail: write id failed");
        return ERR_ANS_PARCELABLE_FAILED;
    }

    MessageParcel reply;
    MessageOption option = {MessageOption::TF_SYNC};
    ErrCode result = InnerTransact(AnsDistributedInterfaceCode::DELETEREMOTENOTIFICATION, option, data, reply);
    if (result != ERR_OK) {
        ANS_LOGE("[DeleteRemoteNotification] fail: transact ErrCode=%{public}d", result);
        return ERR_ANS_TRANSACT_FAILED;
    }

    if (!reply.ReadInt32(result)) {
        ANS_LOGE("[DeleteRemoteNotification] fail: read result failed.");
        return ERR_ANS_PARCELABLE_FAILED;
    }

    return result;
}
    
ErrCode AnsDistributedProxy::Publish(const std::string &bundleName, 
    const std::string &label, int32_t id, const sptr<NotificationRequest> &request)
{
    ANS_LOGD("%{public}s", __FUNCTION__);
    MessageParcel data;
    if (!data.WriteInterfaceToken(AnsDistributedProxy::GetDescriptor())) {
        ANS_LOGE("[Publish] fail: write interface token failed.");
        return ERR_ANS_PARCELABLE_FAILED;
    }

    if (!data.WriteString(bundleName)) {
        ANS_LOGE("[Publish] fail: write bundleName failed");
        return ERR_ANS_PARCELABLE_FAILED;
    }

    if (!data.WriteString(label)) {
        ANS_LOGE("[Publish] fail: write label failed");
        return ERR_ANS_PARCELABLE_FAILED;
    }

    if (!data.WriteInt32(id)) {
        ANS_LOGE("[Publish] fail: write id failed");
        return ERR_ANS_PARCELABLE_FAILED;
    }

    if (!data.WriteParcelable(request)) {
        ANS_LOGE("[Publish] fail: write id failed");
        return ERR_ANS_PARCELABLE_FAILED;
    }

    MessageParcel reply;
    MessageOption option = {MessageOption::TF_SYNC};
    ErrCode result = InnerTransact(AnsDistributedInterfaceCode::DISTRIBUTEDPUBLISH, option, data, reply);
    if (result != ERR_OK) {
        ANS_LOGE("[DeleteRemoteNotification] fail: transact ErrCode=%{public}d", result);
        return ERR_ANS_TRANSACT_FAILED;
    }

    if (!reply.ReadInt32(result)) {
        ANS_LOGE("[DeleteRemoteNotification] fail: read result failed.");
        return ERR_ANS_PARCELABLE_FAILED;
    }

    return result;
}

ErrCode AnsDistributedProxy::CheckRemoteDevicesIsUsing(bool &isUsing)
{
    ANS_LOGD("%{public}s", __FUNCTION__);
    MessageParcel data;
    if (!data.WriteInterfaceToken(AnsDistributedProxy::GetDescriptor())) {
        ANS_LOGE("[CheckRemoteDevicesIsUsing] fail: write interface token failed.");
        return ERR_ANS_PARCELABLE_FAILED;
    }

    if (!data.WriteBool(isUsing)) {
        ANS_LOGE("[CheckRemoteDevicesIsUsing] fail: write deviceId failed");
        return ERR_ANS_PARCELABLE_FAILED;
    }

    MessageParcel reply;
    MessageOption option = {MessageOption::TF_SYNC};
    ErrCode result = InnerTransact(AnsDistributedInterfaceCode::DISTRIBUTEDCHECKREMOTEDEVICESISUSING, option, data, reply);
    if (result != ERR_OK) {
        ANS_LOGE("[CheckRemoteDevicesIsUsing] fail: transact ErrCode=%{public}d", result);
        return ERR_ANS_TRANSACT_FAILED;
    }

    if (!reply.ReadInt32(result)) {
        ANS_LOGE("[CheckRemoteDevicesIsUsing] fail: read result failed.");
        return ERR_ANS_PARCELABLE_FAILED;
    }

    return result;
}

ErrCode AnsDistributedProxy::SetLocalScreenStatus(bool screenOn)
{
    ANS_LOGD("%{public}s", __FUNCTION__);
    MessageParcel data;
    if (!data.WriteInterfaceToken(AnsDistributedProxy::GetDescriptor())) {
        ANS_LOGE("[SetLocalScreenStatus] fail: write interface token failed.");
        return ERR_ANS_PARCELABLE_FAILED;
    }

    bool isUsing = false;
    if (!data.WriteBool(isUsing)) {
        ANS_LOGE("[SetLocalScreenStatus] fail: write deviceId failed");
        return ERR_ANS_PARCELABLE_FAILED;
    }

    MessageParcel reply;
    MessageOption option = {MessageOption::TF_SYNC};
    ErrCode result = InnerTransact(AnsDistributedInterfaceCode::DISTRIBUTEDSETLOCALSCREENSTATUS, option, data, reply);
    if (result != ERR_OK) {
        ANS_LOGE("[SetLocalScreenStatus] fail: transact ErrCode=%{public}d", result);
        return ERR_ANS_TRANSACT_FAILED;
    }

    if (!reply.ReadInt32(result)) {
        ANS_LOGE("[SetLocalScreenStatus] fail: read result failed.");
        return ERR_ANS_PARCELABLE_FAILED;
    }

    return result;
}

ErrCode AnsDistributedProxy::ResetFfrtQueue()
{
    ANS_LOGD("%{public}s", __FUNCTION__);
    MessageParcel data;
    if (!data.WriteInterfaceToken(AnsDistributedProxy::GetDescriptor())) {
        ANS_LOGE("[ResetFfrtQueue] fail: write interface token failed.");
        return ERR_ANS_PARCELABLE_FAILED;
    }

    MessageParcel reply;
    MessageOption option = {MessageOption::TF_SYNC};
    ErrCode result = InnerTransact(AnsDistributedInterfaceCode::DISTRIBUTEDRESETFFRTQUEUE, option, data, reply);
    if (result != ERR_OK) {
        ANS_LOGE("[ResetFfrtQueue] fail: transact ErrCode=%{public}d", result);
        return ERR_ANS_TRANSACT_FAILED;
    }

    if (!reply.ReadInt32(result)) {
        ANS_LOGE("[ResetFfrtQueue] fail: read result failed.");
        return ERR_ANS_PARCELABLE_FAILED;
    }

    return result;
}

ErrCode AnsDistributedProxy::InnerTransact(AnsDistributedInterfaceCode code, MessageOption &flags, MessageParcel &data, MessageParcel &reply)
{
    auto remote = Remote();
    if (remote == nullptr) {
        ANS_LOGE("[InnerTransact] defeat: get Remote defeat code %{public}u", code);
        return ERR_DEAD_OBJECT;
    }
    int32_t err = remote->SendRequest(static_cast<uint32_t>(code), data, reply, flags);
    switch (err) {
        case NO_ERROR: {
            return ERR_OK;
        }
        case DEAD_OBJECT: {
            ANS_LOGE("[InnerTransact] defeat: ipcErr=%{public}d code %{public}d", err, code);
            return ERR_DEAD_OBJECT;
        }
        default: {
            ANS_LOGE("[InnerTransact] defeat: ipcErr=%{public}d code %{public}d", err, code);
            return ERR_ANS_TRANSACT_FAILED;
        }
    }
}
} // namespace OHOS::Notification
