/*
 * Copyright (c) 2021-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "ans_distributed_stub.h"

#include "ans_const_define.h"
#include "ans_inner_errors.h"
#include "ans_log_wrapper.h"
#include "message_option.h"
#include "message_parcel.h"
#include "parcel.h" 

namespace OHOS {
namespace Notification {
const std::map<AnsDistributedInterfaceCode, std::function<ErrCode(AnsDistributedStub *, MessageParcel &, MessageParcel &)>>
    AnsDistributedStub::interfaces_ = {
        {AnsDistributedInterfaceCode::DISTRIBUTEDKVSTOREDEATHRECIPIENT,
            std::bind(&AnsDistributedStub::HandleOnDistributedKvStoreDeathRecipient, std::placeholders::_1, std::placeholders::_2,
                std::placeholders::_3)},
        {AnsDistributedInterfaceCode::DISTRIBUTEDDELETE,
            std::bind(&AnsDistributedStub::HandleDelete, std::placeholders::_1, std::placeholders::_2,
                std::placeholders::_3)},
        {AnsDistributedInterfaceCode::DELETEREMOTENOTIFICATION,
            std::bind(&AnsDistributedStub::HandleDeleteRemoteNotification, std::placeholders::_1, std::placeholders::_2,
                std::placeholders::_3)},
        {AnsDistributedInterfaceCode::DISTRIBUTEDPUBLISH,
            std::bind(&AnsDistributedStub::HandlePublish, std::placeholders::_1, std::placeholders::_2,
                std::placeholders::_3)},
        {AnsDistributedInterfaceCode::DISTRIBUTEDCHECKREMOTEDEVICESISUSING,
            std::bind(&AnsDistributedStub::HandleCheckRemoteDevicesIsUsing, std::placeholders::_1, std::placeholders::_2,
            std::placeholders::_3)},
        {AnsDistributedInterfaceCode::DISTRIBUTEDSETLOCALSCREENSTATUS,
            std::bind(&AnsDistributedStub::HandleSetLocalScreenStatus, std::placeholders::_1, std::placeholders::_2,
            std::placeholders::_3)},
        {AnsDistributedInterfaceCode::DISTRIBUTEDRESETFFRTQUEUE,
            std::bind(&AnsDistributedStub::HandleResetFfrtQueue, std::placeholders::_1, std::placeholders::_2,
            std::placeholders::_3)},
    };

AnsDistributedStub::AnsDistributedStub()
{}

AnsDistributedStub::~AnsDistributedStub()
{}

int32_t AnsDistributedStub::OnRemoteRequest(uint32_t code, MessageParcel &data, MessageParcel &reply, MessageOption &flags)
{
    std::u16string descriptor = AnsDistributedStub::GetDescriptor();
    std::u16string remoteDescriptor = data.ReadInterfaceToken();
    if (descriptor != remoteDescriptor) {
        ANS_LOGE("[OnRemoteRequest] fail: invalid interface token!");
        return OBJECT_NULL;
    }

    auto it = interfaces_.find(static_cast<AnsDistributedInterfaceCode>(code));
    if (it == interfaces_.end()) {
        ANS_LOGE("[OnRemoteRequest] fail: unknown code!");
        return IPCObjectStub::OnRemoteRequest(code, data, reply, flags);
    }

    auto fun = it->second;
    if (fun == nullptr) {
        ANS_LOGE("[OnRemoteRequest] fail: not find function!");
        return IPCObjectStub::OnRemoteRequest(code, data, reply, flags);
    }

    ErrCode result = fun(this, data, reply);
    if (SUCCEEDED(result)) {
        return NO_ERROR;
    }

    ANS_LOGE("[OnRemoteRequest] fail: Failed to call interface %{public}u, err:%{public}d", code, result);
    return result;
}

ErrCode AnsDistributedStub::HandleOnDistributedKvStoreDeathRecipient(MessageParcel &data, MessageParcel &reply)
{
    ANS_LOGE("HandleOnDistributedKvStoreDeathRecipient called!"); 

    ErrCode result = OnDistributedKvStoreDeathRecipient();
    if (!reply.WriteInt32(result)) {
        ANS_LOGE("[HandleOnDistributedKvStoreDeathRecipient] fail: write result failed, ErrCode=%{public}d", result);
        return ERR_ANS_PARCELABLE_FAILED;
    }

    return ERR_OK;   
}

ErrCode AnsDistributedStub::HandleDelete(MessageParcel &data, MessageParcel &reply)
{
    ANS_LOGE("HandleDelete called!");

    std::string bundleName;
    if (!data.ReadString(bundleName)) {
        ANS_LOGE("[HandleDelete] fail: write bundleName failed");
        return ERR_ANS_PARCELABLE_FAILED;
    }

     std::string label;
    if (!data.ReadString(label)) {
        ANS_LOGE("[HandleDelete] fail: write label failed");
        return ERR_ANS_PARCELABLE_FAILED;
    }

    int id = 0;
    if (!data.ReadInt32(id)) {
        ANS_LOGE("[HandleDelete] fail: write id failed");
        return ERR_ANS_PARCELABLE_FAILED;
    }

    ErrCode result = Delete(bundleName, label, id);
    if (!reply.WriteInt32(result)) {
        ANS_LOGE("[HandleDelete] fail: write result failed, ErrCode=%{public}d", result);
        return ERR_ANS_PARCELABLE_FAILED;
    }

    return ERR_OK;       
}

ErrCode AnsDistributedStub::HandleDeleteRemoteNotification(MessageParcel &data, MessageParcel &reply)
{
    ANS_LOGE("HandleDeleteRemoteNotification called!");

    std::string deviceId;
    if (!data.ReadString(deviceId)) {
        ANS_LOGE("[HandleDeleteRemoteNotification] fail: write deviceId failed");
        return ERR_ANS_PARCELABLE_FAILED;
    }

    std::string bundleName;
    if (!data.ReadString(bundleName)) {
        ANS_LOGE("[HandleDeleteRemoteNotification] fail: write bundleName failed");
        return ERR_ANS_PARCELABLE_FAILED;
    }

    std::string label;
    if (!data.ReadString(label)) {
        ANS_LOGE("[HandleDeleteRemoteNotification] fail: write label failed");
        return ERR_ANS_PARCELABLE_FAILED;
    }

    int id = 0;
    if (!data.ReadInt32(id)) {
        ANS_LOGE("[HandleDeleteRemoteNotification] fail: write id failed");
        return ERR_ANS_PARCELABLE_FAILED;
    }

    ErrCode result = DeleteRemoteNotification(deviceId, bundleName, label, id);

    if (!reply.WriteInt32(result)) {
        ANS_LOGE("[HandleDeleteRemoteNotification] fail: write result failed, ErrCode=%{public}d", result);
        return ERR_ANS_PARCELABLE_FAILED;
    }

    return ERR_OK;         
}

ErrCode AnsDistributedStub::HandlePublish(MessageParcel &data, MessageParcel &reply)
{
    ANS_LOGE("HandlePublish called!"); 

    std::string bundleName;
    if (!data.ReadString(bundleName)) {
        ANS_LOGE("[Publish] fail: write bundleName failed");
        return ERR_ANS_PARCELABLE_FAILED;
    }

    std::string label;
    if (!data.ReadString(label)) {
        ANS_LOGE("[Publish] fail: write label failed");
        return ERR_ANS_PARCELABLE_FAILED;
    }

    int id = 0;
    if (!data.ReadInt32(id)) {
        ANS_LOGE("[Publish] fail: write id failed");
        return ERR_ANS_PARCELABLE_FAILED;
    }

    sptr<NotificationRequest> request = data.ReadParcelable<NotificationRequest>();
    if (!request) {
        ANS_LOGE("[Publish] fail: request ReadParcelable failed");
        return ERR_ANS_PARCELABLE_FAILED;
    }

    ErrCode result = Publish(bundleName, label, id, request);

    if (!reply.WriteInt32(result)) {
        ANS_LOGE("[HandleDeleteRemoteNotification] fail: write result failed, ErrCode=%{public}d", result);
        return ERR_ANS_PARCELABLE_FAILED;
    }

    return ERR_OK;       
}

ErrCode AnsDistributedStub::HandleCheckRemoteDevicesIsUsing(MessageParcel &data, MessageParcel &reply)
{
    bool isUsing = false;
    if (!data.ReadBool(isUsing)) {
        ANS_LOGE("[Publish] fail: write bundleName failed");
        return ERR_ANS_PARCELABLE_FAILED;
    }

    ErrCode result = CheckRemoteDevicesIsUsing(isUsing);

    if (!reply.WriteInt32(result)) {
        ANS_LOGE("[HandleDeleteRemoteNotification] fail: write result failed, ErrCode=%{public}d", result);
        return ERR_ANS_PARCELABLE_FAILED;
    }
    
    return result;
}

ErrCode AnsDistributedStub::HandleSetLocalScreenStatus(MessageParcel &data, MessageParcel &reply)
{
    bool screenOn = false;
    if (!data.ReadBool(screenOn)) {
        ANS_LOGE("[Publish] fail: write bundleName failed");
        return ERR_ANS_PARCELABLE_FAILED;
    }

    ErrCode result = SetLocalScreenStatus(screenOn);

    if (!reply.WriteInt32(result)) {
        ANS_LOGE("[HandleDeleteRemoteNotification] fail: write result failed, ErrCode=%{public}d", result);
        return ERR_ANS_PARCELABLE_FAILED;
    }

    return result;
}

ErrCode AnsDistributedStub::HandleResetFfrtQueue(MessageParcel &data, MessageParcel &reply)
{
    ErrCode result = ResetFfrtQueue();

    if (!reply.WriteInt32(result)) {
        ANS_LOGE("[HandleDeleteRemoteNotification] fail: write result failed, ErrCode=%{public}d", result);
        return ERR_ANS_PARCELABLE_FAILED;
    }

    return result;
}

ErrCode AnsDistributedStub::OnDistributedKvStoreDeathRecipient()
{
    ANS_LOGE("OnDistributedKvStoreDeathRecipient called!");
    return ERR_OK; 
}

ErrCode AnsDistributedStub::Delete(const std::string &bundleName, const std::string &label, int32_t id)
{
    ANS_LOGE("Delete called!");
    return ERR_OK; 
}

ErrCode AnsDistributedStub::DeleteRemoteNotification(const std::string &deviceId,
    const std::string &bundleName, const std::string &label, int32_t id)
{
    ANS_LOGE("DeleteRemoteNotification called!");
    return ERR_OK; 
}
  
ErrCode AnsDistributedStub::Publish(const std::string &bundleName,
    const std::string &label, int32_t id, const sptr<NotificationRequest> &request)
{
    ANS_LOGE("Publish called!");
    return ERR_OK; 
}

ErrCode AnsDistributedStub::CheckRemoteDevicesIsUsing(bool &isUsing)
{
    ANS_LOGE("CheckRemoteDevicesIsUsing called!");
    return ERR_OK; 
}

ErrCode AnsDistributedStub::SetLocalScreenStatus(bool screenOn)
{
    ANS_LOGE("SetLocalScreenStatus called!");
    return ERR_OK; 
}

ErrCode AnsDistributedStub::ResetFfrtQueue() 
{
    ANS_LOGE("ResetFfrtQueue called!");
    return ERR_OK; 
}

}  // namespace Notification
}  // namespace OHOS
