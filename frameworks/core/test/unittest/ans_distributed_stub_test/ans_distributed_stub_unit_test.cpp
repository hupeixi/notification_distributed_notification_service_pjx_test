/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <gtest/gtest.h>

#define private public
#define protected public
#include "ans_distributed_stub.h"
#include "ans_inner_errors.h"
#include "ans_notification.h"
#undef private
#undef protected
#include "ans_distributed_interface.h"
#include "ans_const_define.h"
#include "message_option.h"
#include "message_parcel.h"
#include "notification.h"
#include "parcel.h"
#include "mock_i_remote_object.h"



using namespace testing;
using namespace testing::ext;
namespace OHOS {
namespace Notification {
class AnsDistributedStubUnitTest : public testing::Test {
public:
    AnsDistributedStubUnitTest() {}

    virtual ~AnsDistributedStubUnitTest() {}

    static void SetUpTestCase();

    static void TearDownTestCase();

    void SetUp() override;

    void TearDown() override;

    sptr<AnsDistributedStub> stub_;
};

void AnsDistributedStubUnitTest::SetUpTestCase()
{
}

void AnsDistributedStubUnitTest::TearDownTestCase()
{
}

void AnsDistributedStubUnitTest::SetUp()
{
    stub_ = new AnsDistributedStub();
}

void AnsDistributedStubUnitTest::TearDown()
{
}

/**
 * @tc.name: OnRemoteRequest01
 * @tc.desc: Test if get the wrong descriptor.
 * @tc.type: FUNC
 */
HWTEST_F(AnsDistributedStubUnitTest, OnRemoteRequest0001, Function | SmallTest | Level1)
{
    uint32_t code = static_cast<uint32_t>(NotificationInterfaceCode::PUBLISH_NOTIFICATION);
    MessageParcel data;
    MessageParcel reply;
    MessageOption option = {MessageOption::TF_SYNC};

    data.WriteInterfaceToken(u"error.GetDescriptor");

    ErrCode ret = stub_->OnRemoteRequest(code, data, reply, option);
    EXPECT_EQ(ret, (int)OBJECT_NULL);
}

/**
 * @tc.name: OnRemoteRequest02
 * @tc.desc: Test if get the wrong code.
 * @tc.type: FUNC
 */
HWTEST_F(AnsDistributedStubUnitTest, OnRemoteRequest0002, Function | SmallTest | Level1)
{
    uint32_t code = 267;
    MessageParcel data;
    MessageParcel reply;
    MessageOption option = {MessageOption::TF_SYNC};

    data.WriteInterfaceToken(AnsDistributedStub::GetDescriptor());

    ErrCode ret = stub_->OnRemoteRequest(code, data, reply, option);
    EXPECT_EQ(ret, (int)IPC_STUB_UNKNOW_TRANS_ERR);
}

/**
* @tc.name: HandleOnDistributedKvStoreDeathRecipient
* @tc.desc: test HandleOnDistributedKvStoreDeathRecipient success
* @tc.type: Fun
*/
HWTEST_F(AnsDistributedStubUnitTest, HandleOnDistributedKvStoreDeathRecipient, Function | SmallTest | Level2)
{
    MessageParcel data;
    MessageParcel reply;

    ErrCode res = stub_->HandleOnDistributedKvStoreDeathRecipient(data, reply);
    EXPECT_EQ(res, ERR_OK);
}

/**
* @tc.name: HandleDelete01
* @tc.desc: test HandleDelete return 
* @tc.type: Fun
*/
HWTEST_F(AnsDistributedStubUnitTest, HandleDelete01, Function | SmallTest | Level2)
{
    MessageParcel data;
    MessageParcel reply;

    ErrCode res = stub_->HandleDelete(data, reply);
    EXPECT_EQ(res, ERR_ANS_PARCELABLE_FAILED);
}

/**
* @tc.name: HandleDelete02
* @tc.desc: test HandleDelete return 
* @tc.type: Fun
*/
HWTEST_F(AnsDistributedStubUnitTest, HandleDelete02, Function | SmallTest | Level2)
{
    MessageParcel data;
    MessageParcel reply;
    MessageOption option = {MessageOption::TF_SYNC};

    std::string bundleName = "test_bundlename";
    data.WriteString(bundleName);
    ErrCode res = stub_->HandleDelete(data, reply);
    EXPECT_EQ(res, ERR_ANS_PARCELABLE_FAILED);
}

/**
* @tc.name: HandleDelete03
* @tc.desc: test HandleDelete return 
* @tc.type: Fun
*/
HWTEST_F(AnsDistributedStubUnitTest, HandleDelete03, Function | SmallTest | Level2)
{
    MessageParcel data;
    MessageParcel reply;
    MessageOption option = {MessageOption::TF_SYNC};

    std::string bundleName = "test_bundlename";
    std::string label = "this is test label";
    data.WriteString(bundleName);
    data.WriteString(label);
    ErrCode res = stub_->HandleDelete(data, reply);
    EXPECT_EQ(res, ERR_ANS_PARCELABLE_FAILED);
}

/**
* @tc.name: HandleDelete04
* @tc.desc: test HandleDelete return 
* @tc.type: Fun
*/
HWTEST_F(AnsDistributedStubUnitTest, HandleDelete04, Function | SmallTest | Level2)
{
    MessageParcel data;
    MessageParcel reply;
    MessageOption option = {MessageOption::TF_SYNC};

    int32_t id = 0;
    std::string bundleName = "test_bundlename";
    std::string label = "this is test label";
    data.WriteString(bundleName);
    data.WriteString(label);
    data.WriteInt32(id);
    ErrCode res = stub_->HandleDelete(data, reply);
    EXPECT_EQ(res, ERR_OK);
}

/**
* @tc.name: HandleDeleteRemoteNotification01
* @tc.desc: test HandleDeleteRemoteNotification return 
* @tc.type: Fun
*/

HWTEST_F(AnsDistributedStubUnitTest, HandleDeleteRemoteNotification01, Function | SmallTest | Level2)
{
    MessageParcel data;
    MessageParcel reply;

    ErrCode res = stub_->HandleDeleteRemoteNotification(data, reply);
    EXPECT_EQ(res, ERR_ANS_PARCELABLE_FAILED);
}

/**
* @tc.name: HandleDeleteRemoteNotification02
* @tc.desc: test HandleDeleteRemoteNotification return
* @tc.type: Fun
*/
HWTEST_F(AnsDistributedStubUnitTest, HandleDeleteRemoteNotification02, Function | SmallTest | Level2)
{
    MessageParcel data;
    MessageParcel reply;

    std::string deviceId = "this is test id";
    date.WriteString(deviceId);
    ErrCode res = stub_->HandleDeleteRemoteNotification(data, reply);
    EXPECT_EQ(res, ERR_ANS_PARCELABLE_FAILED);
}

/**
* @tc.name: HandleDeleteRemoteNotification03
* @tc.desc: test HandleDeleteRemoteNotification return
* @tc.type: Fun
*/
HWTEST_F(AnsDistributedStubUnitTest, HandleDeleteRemoteNotification03, Function | SmallTest | Level2)
{
    MessageParcel data;
    MessageParcel reply;

    std::string deviceId = "this is test id";
    std::string bundleName = "this is test name"; 
    date.WriteString(deviceId);
    date.WriteString(bundleName);
    ErrCode res = stub_->HandleDeleteRemoteNotification(data, reply);
    EXPECT_EQ(res, ERR_ANS_PARCELABLE_FAILED);
}

/**
* @tc.name: HandleDeleteRemoteNotification04
* @tc.desc: test HandleDeleteRemoteNotification return 
* @tc.type: Fun
*/
HWTEST_F(AnsDistributedStubUnitTest, HandleDeleteRemoteNotification04, Function | SmallTest | Level2)
{
    MessageParcel data;
    MessageParcel reply;

    std::string deviceId = "this is test id";
    std::string bundleName = "this is test name"; 
    std::string label = "this is test label";
    date.WriteString(deviceId);
    date.WriteString(bundleName);
    date.WriteString(label);
    ErrCode res = stub_->HandleDeleteRemoteNotification(data, reply);
    EXPECT_EQ(res, ERR_ANS_PARCELABLE_FAILED);
}

/**
* @tc.name: HandleDeleteRemoteNotification05
* @tc.desc: test HandleDeleteRemoteNotification return 
* @tc.type: Fun
*/
HWTEST_F(AnsDistributedStubUnitTest, HandleDeleteRemoteNotification05, Function | SmallTest | Level2)
{
    MessageParcel data;
    MessageParcel reply;

    int32_t id = 0;
    std::string deviceId = "this is test id";
    std::string bundleName = "this is test name"; 
    std::string label = "this is test label";
    date.WriteString(deviceId);
    date.WriteString(bundleName);
    date.WriteString(label);
    date.WriteInt32(id);
    ErrCode res = stub_->HandleDeleteRemoteNotification(data, reply);
    EXPECT_EQ(res, ERR_OK);
}

/**
* @tc.name: HandlePublish01
* @tc.desc: test HandlePublish return 
* @tc.type: Fun
*/
HWTEST_F(AnsDistributedStubUnitTest, HandlePublish01, Function | SmallTest | Level2)
{
    MessageParcel data;
    MessageParcel reply;

    ErrCode res = stub_->HandlePublish(data, reply);
    EXPECT_EQ(res, ERR_ANS_PARCELABLE_FAILED);
}


/**
* @tc.name: HandlePublish02
* @tc.desc: test HandlePublish return 
* @tc.type: Fun
*/
HWTEST_F(AnsDistributedStubUnitTest, HandlePublish02, Function | SmallTest | Level2)
{
    MessageParcel data;
    MessageParcel reply;

    std::string bundleName = "this is test name"; 
    date.WriteString(bundleName);
    ErrCode res = stub_->HandlePublish(data, reply);
    EXPECT_EQ(res, ERR_ANS_PARCELABLE_FAILED);
}

/**
* @tc.name: HandlePublish03
* @tc.desc: test HandlePublish return 
* @tc.type: Fun
*/
HWTEST_F(AnsDistributedStubUnitTest, HandlePublish03, Function | SmallTest | Level2)
{
    MessageParcel data;
    MessageParcel reply;

    std::string label = "this is test label";
    std::string bundleName = "this is test name"; 
    date.WriteString(bundleName);
    date.WriteString(label);
    ErrCode res = stub_->HandlePublish(data, reply);
    EXPECT_EQ(res, ERR_ANS_PARCELABLE_FAILED);
}

/**
* @tc.name: HandlePublish04
* @tc.desc: test HandlePublish return
* @tc.type: Fun
*/
HWTEST_F(AnsDistributedStubUnitTest, HandlePublish04, Function | SmallTest | Level2)
{
    MessageParcel data;
    MessageParcel reply;

    int32_t id = 0;
    std::string label = "this is test label";
    std::string bundleName = "this is test name"; 
    date.WriteString(bundleName);
    date.WriteString(label);
    date.WriteInt32(id);
    ErrCode res = stub_->HandlePublish(data, reply);
    EXPECT_EQ(res, ERR_ANS_PARCELABLE_FAILED);
}

/**
* @tc.name: HandlePublish05
* @tc.desc: test HandlePublish return 
* @tc.type: Fun
*/
HWTEST_F(AnsDistributedStubUnitTest, HandlePublish05, Function | SmallTest | Level2)
{
    MessageParcel data;
    MessageParcel reply;
    sptr<NotificationRequest> request = new NotificationRequest();
    int32_t id = 0;
    std::string label = "this is test label";
    std::string bundleName = "this is test name"; 
    date.WriteString(bundleName);
    date.WriteString(label);
    date.WriteInt32(id);
    data.WriteParcelable(request);
    ErrCode res = stub_->HandlePublish(data, reply);
    EXPECT_EQ(res, ERR_ANS_PARCELABLE_FAILED);
}

/**
* @tc.name: HandleCheckRemoteDevicesIsUsing01
* @tc.desc: test HandleCheckRemoteDevicesIsUsing return 
* @tc.type: Fun
*/
HWTEST_F(AnsDistributedStubUnitTest, HandleCheckRemoteDevicesIsUsing01, Function | SmallTest | Level2)
{
    MessageParcel data;
    MessageParcel reply;

    ErrCode res = stub_->HandleCheckRemoteDevicesIsUsing(data, reply);
    EXPECT_EQ(res, ERR_ANS_PARCELABLE_FAILED);
}

/**
* @tc.name: HandleCheckRemoteDevicesIsUsing02
* @tc.desc: test HandleCheckRemoteDevicesIsUsing return 
* @tc.type: Fun
*/
HWTEST_F(AnsDistributedStubUnitTest, HandleCheckRemoteDevicesIsUsing02, Function | SmallTest | Level2)
{
    MessageParcel data;
    MessageParcel reply;

    bool isUsing = false;
    data.WriteBool(isUsing);
    ErrCode res = stub_->HandleCheckRemoteDevicesIsUsing(data, reply);
    EXPECT_EQ(res, ERR_OK);
}

/**
* @tc.name: HandleSetLocalScreenStatus01
* @tc.desc: test HandleSetLocalScreenStatus return 
* @tc.type: Fun
*/
HWTEST_F(AnsDistributedStubUnitTest, HandleSetLocalScreenStatus01, Function | SmallTest | Level2)
{
    MessageParcel data;
    MessageParcel reply;

    ErrCode res = stub_->HandleSetLocalScreenStatus(data, reply);
    EXPECT_EQ(res, ERR_ANS_PARCELABLE_FAILED);
}

/**
* @tc.name: HandleSetLocalScreenStatus02
* @tc.desc: test HandleSetLocalScreenStatus return
* @tc.type: Fun
*/
HWTEST_F(AnsDistributedStubUnitTest, HandleSetLocalScreenStatus02, Function | SmallTest | Level2)
{
    MessageParcel data;
    MessageParcel reply;

    bool screenOn = false;
    data.WriteBool(screenOn);
    ErrCode res = stub_->HandleSetLocalScreenStatus(data, reply);
    EXPECT_EQ(res, ERR_OK);
}

/**
* @tc.name: HandleResetFfrtQueue
* @tc.desc: test HandleResetFfrtQueue return
* @tc.type: Fun
*/
HWTEST_F(AnsDistributedStubUnitTest, HandleResetFfrtQueue, Function | SmallTest | Level2)
{
    MessageParcel data;
    MessageParcel reply;

    ErrCode res = stub_->HandleResetFfrtQueue(data, reply);
    EXPECT_EQ(res, ERR_OK);
}

/**
 * @tc.name: OnDistributedKvStoreDeathRecipient
 * @tc.desc: Test OnDistributedKvStoreDeathRecipient return.
 * @tc.type: FUNC
 */
HWTEST_F(AnsDistributedStubUnitTest, OnDistributedKvStoreDeathRecipient, Function | SmallTest | Level1)
{
    ErrCode result = stub_->OnDistributedKvStoreDeathRecipient();
    EXPECT_EQ(result, (int)ERR_OK);
}

/**
 * @tc.name: Delete
 * @tc.desc: Test Delete return.
 * @tc.type: FUNC
 */
HWTEST_F(AnsDistributedStubUnitTest, Delete, Function | SmallTest | Level1)
{
    int32_t id = 0; 
    std::string bundleName = "test";
    std::string label = "test_label";
    ErrCode result = stub_->Delete(bundleName, label, id);
    EXPECT_EQ(result, (int)ERR_OK);
}

/**
 * @tc.name: DeleteRemoteNotification
 * @tc.desc: Test DeleteRemoteNotification return.
 * @tc.type: FUNC
 */
HWTEST_F(AnsDistributedStubUnitTest, DeleteRemoteNotification, Function | SmallTest | Level1)
{
    int32_t id = 0; 
    std::string label = "test_lable";
    std::string deviceId = "test_Id";
    std::string bundleName = "test_bundle";
    ErrCode result = stub_->DeleteRemoteNotification(deviceId, bundleName, label, id);
    EXPECT_EQ(result, (int)ERR_OK);
}

/**
 * @tc.name: Publish
 * @tc.desc: Test Publish return.
 * @tc.type: FUNC
 */
HWTEST_F(AnsDistributedStubUnitTest, Publish, Function | SmallTest | Level1)
{
    int32_t id = 0; 
    std::string label = "test_lable";
    std::string deviceId = "test_Id";
    std::string bundleName = "test_bundle";
    sptr<NotificationRequest> request = nullptr;
    ErrCode result = stub_->Publish(bundleName, label, id, request);
    EXPECT_EQ(result, (int)ERR_OK);
}

/**
 * @tc.name: CheckRemoteDevicesIsUsing
 * @tc.desc: Test CheckRemoteDevicesIsUsing return.
 * @tc.type: FUNC
 */
HWTEST_F(AnsDistributedStubUnitTest, CheckRemoteDevicesIsUsing, Function | SmallTest | Level1)
{
    bool isUsing = false; 
    ErrCode result = stub_->CheckRemoteDevicesIsUsing(isUsing);
    EXPECT_EQ(result, (int)ERR_OK);
}

/**
 * @tc.name: SetLocalScreenStatus
 * @tc.desc: Test SetLocalScreenStatus return.
 * @tc.type: FUNC
 */
HWTEST_F(AnsDistributedStubUnitTest, SetLocalScreenStatus, Function | SmallTest | Level1)
{
    bool isUsing = false; 
    ErrCode result = stub_->SetLocalScreenStatus(isUsing);
    EXPECT_EQ(result, (int)ERR_OK);
}

/**
 * @tc.name: ResetFfrtQueue
 * @tc.desc: Test ResetFfrtQueue return.
 * @tc.type: FUNC
 */
HWTEST_F(AnsDistributedStubUnitTest, ResetFfrtQueue, Function | SmallTest | Level1)
{
    ErrCode result = stub_->ResetFfrtQueue();
    EXPECT_EQ(result, (int)ERR_OK);
}

}
}
