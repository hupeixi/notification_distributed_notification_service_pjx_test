/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "notification_request.h"
#include <gtest/gtest.h>

#define private public
#define protected public
#include "ans_distributed_proxy.h"
#undef private
#undef protected
#include "ans_inner_errors.h"
#include "ipc_types.h"
#include "mock_i_remote_object.h"
#include "notification.h"

using namespace testing;
using namespace testing::ext;
using namespace OHOS;
using namespace OHOS::Notification;

class AnsDistributedProxyUnitTest : public testing::Test {
public:
    AnsDistributedProxyUnitTest() {}

    virtual ~AnsDistributedProxyUnitTest() {}

    static void SetUpTestCase();

    static void TearDownTestCase();

    void SetUp();

    void TearDown();
};

void AnsDistributedProxyUnitTest::SetUpTestCase() {}

void AnsDistributedProxyUnitTest::TearDownTestCase() {}

void AnsDistributedProxyUnitTest::SetUp() {}

void AnsDistributedProxyUnitTest::TearDown() {}

/*
 * @tc.name: InnerTransactTest_0100
 * @tc.desc: test if AnsDistributedProxy's InnerTransact function executed as expected in normal case.
 * @tc.type: FUNC
 */
HWTEST_F(AnsDistributedProxyUnitTest, InnerTransactTest_0100, Function | MediumTest | Level1)
{
    GTEST_LOG_(INFO)
        << "AnsDistributedProxyUnitTest, InnerTransactTest_0100, TestSize.Level1";
    sptr<MockIRemoteObject> iremoteObject = new (std::nothrow) MockIRemoteObject();
    ASSERT_NE(nullptr, iremoteObject);
    EXPECT_CALL(*iremoteObject, SendRequest(_, _, _, _)).WillOnce(DoAll(Return(NO_ERROR)));
    std::shared_ptr<AnsDistributedProxy> proxy = std::make_shared<AnsDistributedProxy>(iremoteObject);
    ASSERT_NE(nullptr, proxy);
    uint32_t code = 0;
    MessageOption flags;
    MessageParcel data;
    MessageParcel reply;
    ErrCode res = proxy->InnerTransact(static_cast<NotificationInterfaceCode>(code), flags, data, reply);
    EXPECT_EQ(ERR_OK, res);
}

/*
 * @tc.name: InnerTransactTest_0200
 * @tc.desc: test AnsDistributedProxy's InnerTransact function
 * @tc.type: FUNC
 */
HWTEST_F(AnsDistributedProxyUnitTest, InnerTransactTest_0200, Function | MediumTest | Level1)
{
    GTEST_LOG_(INFO)
        << "AnsDistributedProxyUnitTest, InnerTransactTest_0200, TestSize.Level1";
    sptr<MockIRemoteObject> iremoteObject = new (std::nothrow) MockIRemoteObject();
    ASSERT_NE(nullptr, iremoteObject);
    EXPECT_CALL(*iremoteObject, SendRequest(_, _, _, _)).WillOnce(DoAll(Return(DEAD_OBJECT)));
    std::shared_ptr<AnsDistributedProxy> proxy = std::make_shared<AnsDistributedProxy>(iremoteObject);
    ASSERT_NE(nullptr, proxy);
    uint32_t code = 0;
    MessageOption flags;
    MessageParcel data;
    MessageParcel reply;
    ErrCode res = proxy->InnerTransact(static_cast<NotificationInterfaceCode>(code), flags, data, reply);
    EXPECT_EQ(ERR_DEAD_OBJECT, res);
}

/*
 * @tc.name: InnerTransactTest_0300
 * @tc.desc: test AnsDistributedProxy's InnerTransact function
 * @tc.type: FUNC
 */
HWTEST_F(AnsDistributedProxyUnitTest, InnerTransactTest_0300, Function | MediumTest | Level1)
{
    GTEST_LOG_(INFO)
        << "AnsDistributedProxyUnitTest, InnerTransactTest_0300, TestSize.Level1";
    sptr<MockIRemoteObject> iremoteObject = new (std::nothrow) MockIRemoteObject();
    ASSERT_NE(nullptr, iremoteObject);
    EXPECT_CALL(*iremoteObject, SendRequest(_, _, _, _)).WillOnce(DoAll(Return(-1)));
    std::shared_ptr<AnsDistributedProxy> proxy = std::make_shared<AnsDistributedProxy>(iremoteObject);
    ASSERT_NE(nullptr, proxy);
    uint32_t code = 0;
    MessageOption flags;
    MessageParcel data;
    MessageParcel reply;
    ErrCode res = proxy->InnerTransact(static_cast<NotificationInterfaceCode>(code), flags, data, reply);
    EXPECT_EQ(ERR_ANS_TRANSACT_FAILED, res);
}

/*
 * @tc.name: InnerTransactTest_0400
 * @tc.desc: test AnsDistributedProxy's InnerTransact function
 * @tc.type: FUNC
 */
HWTEST_F(AnsDistributedProxyUnitTest, InnerTransactTest_0400, Function | MediumTest | Level1)
{
    GTEST_LOG_(INFO)
        << "AnsDistributedProxyUnitTest, InnerTransactTest_0400, TestSize.Level1";
    std::shared_ptr<AnsDistributedProxy> proxy = std::make_shared<AnsDistributedProxy>(nullptr);
    ASSERT_NE(nullptr, proxy);
    uint32_t code = 0;
    MessageOption flags;
    MessageParcel data;
    MessageParcel reply;
    ErrCode res = proxy->InnerTransact(static_cast<NotificationInterfaceCode>(code), flags, data, reply);
    EXPECT_EQ(ERR_DEAD_OBJECT, res);
}

/*
 * @tc.name: OnDistributedKvStoreDeathRecipient_0100
 * @tc.desc: test AnsDistributedProxy's OnDistributedKvStoreDeathRecipient function
 * @tc.type: FUNC
 */
HWTEST_F(AnsDistributedProxyUnitTest, OnDistributedKvStoreDeathRecipient_0100, Function | MediumTest | Level1)
{
    GTEST_LOG_(INFO)
        << "AnsDistributedProxyUnitTest, OnDistributedKvStoreDeathRecipient_0100, TestSize.Level1";
    sptr<MockIRemoteObject> iremoteObject = new (std::nothrow) MockIRemoteObject();
    ASSERT_NE(nullptr, iremoteObject);
    EXPECT_CALL(*iremoteObject, SendRequest(_, _, _, _)).Times(1).WillRepeatedly(DoAll(Return(NO_ERROR)));
    std::shared_ptr<AnsDistributedProxy> proxy = std::make_shared<AnsDistributedProxy>(iremoteObject);
    ASSERT_NE(nullptr, proxy);
    
    ErrCode res = proxy->OnDistributedKvStoreDeathRecipient();
    EXPECT_EQ(res, ERR_OK);
}

/*
 * @tc.name: OnDistributedKvStoreDeathRecipient_0200
 * @tc.desc: test AnsDistributedProxy's OnDistributedKvStoreDeathRecipient function
 * @tc.type: FUNC
 */
HWTEST_F(AnsDistributedProxyUnitTest, OnDistributedKvStoreDeathRecipient_0200, Function | MediumTest | Level1)
{
    GTEST_LOG_(INFO)
        << "AnsDistributedProxyUnitTest, OnDistributedKvStoreDeathRecipient_0200, TestSize.Level1";
    sptr<MockIRemoteObject> iremoteObject = new (std::nothrow) MockIRemoteObject();
    ASSERT_NE(nullptr, iremoteObject);
    EXPECT_CALL(*iremoteObject, SendRequest(_, _, _, _)).Times(1).WillRepeatedly(DoAll(Return(DEAD_OBJECT)));
    std::shared_ptr<AnsDistributedProxy> proxy = std::make_shared<AnsDistributedProxy>(iremoteObject);
    ASSERT_NE(nullptr, proxy);
    
    ErrCode res = proxy->OnDistributedKvStoreDeathRecipient();
    EXPECT_EQ(res, ERR_DEAD_OBJECT);
}

/*
 * @tc.name: Delete_0100
 * @tc.desc: test AnsDistributedProxy's Delete function
 * @tc.type: FUNC
 */
HWTEST_F(AnsDistributedProxyUnitTest, Delete_0100, Function | MediumTest | Level1)
{
    GTEST_LOG_(INFO)
        << "AnsDistributedProxyUnitTest, Delete_0100, TestSize.Level1";
    sptr<MockIRemoteObject> iremoteObject = new (std::nothrow) MockIRemoteObject();
    ASSERT_NE(nullptr, iremoteObject);
    EXPECT_CALL(*iremoteObject, SendRequest(_, _, _, _)).Times(1).WillRepeatedly(DoAll(Return(DEAD_OBJECT)));
    std::shared_ptr<AnsDistributedProxy> proxy = std::make_shared<AnsDistributedProxy>(iremoteObject);
    ASSERT_NE(nullptr, proxy);
    
    int32_t id = 0;
    std::string label = "label";
    std::string bundleName = "<bundlename>";
    ErrCode res = proxy->Delete(bundleName, label, label);
    EXPECT_EQ(res, ERR_DEAD_OBJECT);
}

/*
 * @tc.name: Delete_0200
 * @tc.desc: test AnsDistributedProxy's Delete function
 * @tc.type: FUNC
 */
HWTEST_F(AnsDistributedProxyUnitTest, Delete_0200, Function | MediumTest | Level1)
{
    GTEST_LOG_(INFO)
        << "AnsDistributedProxyUnitTest, Delete_0200, TestSize.Level1";
    sptr<MockIRemoteObject> iremoteObject = new (std::nothrow) MockIRemoteObject();
    ASSERT_NE(nullptr, iremoteObject);
    EXPECT_CALL(*iremoteObject, SendRequest(_, _, _, _)).Times(1).WillRepeatedly(DoAll(Return(NO_ERROR)));
    std::shared_ptr<AnsDistributedProxy> proxy = std::make_shared<AnsDistributedProxy>(iremoteObject);
    ASSERT_NE(nullptr, proxy);
    
    int32_t id = 0;
    std::string label = "label";
    std::string bundleName = "<bundlename>";
    ErrCode res = proxy->Delete(bundleName, label, label);
    EXPECT_EQ(res, ERR_OK);
}

/*
 * @tc.name: DeleteRemoteNotification_0100
 * @tc.desc: test AnsDistributedProxy's DeleteRemoteNotification function
 * @tc.type: FUNC
 */
HWTEST_F(AnsDistributedProxyUnitTest, DeleteRemoteNotification_0100, Function | MediumTest | Level1)
{
    GTEST_LOG_(INFO)
        << "AnsDistributedProxyUnitTest, DeleteRemoteNotification_0100, TestSize.Level1";
    sptr<MockIRemoteObject> iremoteObject = new (std::nothrow) MockIRemoteObject();
    ASSERT_NE(nullptr, iremoteObject);
    EXPECT_CALL(*iremoteObject, SendRequest(_, _, _, _)).Times(1).WillRepeatedly(DoAll(Return(NO_ERROR)));
    std::shared_ptr<AnsDistributedProxy> proxy = std::make_shared<AnsDistributedProxy>(iremoteObject);
    ASSERT_NE(nullptr, proxy);
    
    int32_t id = 0;
    std::string label = "label";
    std::string deviceId = "<deviceId>"
    std::string bundleName = "<bundlename>";
    ErrCode res = proxy->DeleteRemoteNotification(deviceId, bundleName, label, id);
    EXPECT_EQ(res, ERR_OK);
}

/*
 * @tc.name: DeleteRemoteNotification_0200
 * @tc.desc: test AnsDistributedProxy's DeleteRemoteNotification function
 * @tc.type: FUNC
 */
HWTEST_F(AnsDistributedProxyUnitTest, DeleteRemoteNotification_0200, Function | MediumTest | Level1)
{
    GTEST_LOG_(INFO)
        << "AnsDistributedProxyUnitTest, DeleteRemoteNotification_0200, TestSize.Level1";
    sptr<MockIRemoteObject> iremoteObject = new (std::nothrow) MockIRemoteObject();
    ASSERT_NE(nullptr, iremoteObject);
    EXPECT_CALL(*iremoteObject, SendRequest(_, _, _, _)).Times(1).WillRepeatedly(DoAll(Return(DEAD_OBJECT)));
    std::shared_ptr<AnsDistributedProxy> proxy = std::make_shared<AnsDistributedProxy>(iremoteObject);
    ASSERT_NE(nullptr, proxy);
    
    int32_t id = 0;
    std::string label = "label";
    std::string deviceId = "<deviceId>"
    std::string bundleName = "<bundlename>";
    ErrCode res = proxy->DeleteRemoteNotification(deviceId, bundleName, label, id);
    EXPECT_EQ(res, ERR_DEAD_OBJECT);
}

/*
 * @tc.name: Publish_0100
 * @tc.desc: test AnsDistributedProxy's Publish function
 * @tc.type: FUNC
 */
HWTEST_F(AnsDistributedProxyUnitTest, Publish_0100, Function | MediumTest | Level1)
{
    GTEST_LOG_(INFO)
        << "AnsDistributedProxyUnitTest, Publish_0100, TestSize.Level1";
    sptr<MockIRemoteObject> iremoteObject = new (std::nothrow) MockIRemoteObject();
    ASSERT_NE(nullptr, iremoteObject);
    EXPECT_CALL(*iremoteObject, SendRequest(_, _, _, _)).Times(1).WillRepeatedly(DoAll(Return(DEAD_OBJECT)));
    std::shared_ptr<AnsDistributedProxy> proxy = std::make_shared<AnsDistributedProxy>(iremoteObject);
    ASSERT_NE(nullptr, proxy);
    
    int32_t id = 0;
    std::string label = "label";
    std::string bundleName = "<bundlename>";
    sptr<OHOS::Notification::NotificationRequest> request =
        new (std::nothrow) OHOS::Notification::NotificationRequest();
    ASSERT_NE(nullptr, request);
    ErrCode res = proxy->Publish(bundleName, label, id, request);
    EXPECT_EQ(res, ERR_DEAD_OBJECT);
}

/*
 * @tc.name: Publish_0200
 * @tc.desc: test AnsDistributedProxy's Publish function
 * @tc.type: FUNC
 */
HWTEST_F(AnsDistributedProxyUnitTest, Publish_0200, Function | MediumTest | Level1)
{
    GTEST_LOG_(INFO)
        << "AnsDistributedProxyUnitTest, Publish_0200, TestSize.Level1";
    sptr<MockIRemoteObject> iremoteObject = new (std::nothrow) MockIRemoteObject();
    ASSERT_NE(nullptr, iremoteObject);
    EXPECT_CALL(*iremoteObject, SendRequest(_, _, _, _)).Times(1).WillRepeatedly(DoAll(Return(NO_ERROR)));
    std::shared_ptr<AnsDistributedProxy> proxy = std::make_shared<AnsDistributedProxy>(iremoteObject);
    ASSERT_NE(nullptr, proxy);
    
    int32_t id = 0;
    std::string label = "label";
    std::string bundleName = "<bundlename>";
    sptr<OHOS::Notification::NotificationRequest> request =
        new (std::nothrow) OHOS::Notification::NotificationRequest();
    ASSERT_NE(nullptr, request);
    ErrCode res = proxy->Publish(bundleName, label, id, request);
    EXPECT_EQ(res, ERR_OK);
}

/*
 * @tc.name: CheckRemoteDevicesIsUsing_0100
 * @tc.desc: test AnsDistributedProxy's CheckRemoteDevicesIsUsing function
 * @tc.type: FUNC
 */
HWTEST_F(AnsDistributedProxyUnitTest, CheckRemoteDevicesIsUsing_0100, Function | MediumTest | Level1)
{
    GTEST_LOG_(INFO)
        << "AnsDistributedProxyUnitTest, CheckRemoteDevicesIsUsing_0100, TestSize.Level1";
    sptr<MockIRemoteObject> iremoteObject = new (std::nothrow) MockIRemoteObject();
    ASSERT_NE(nullptr, iremoteObject);
    EXPECT_CALL(*iremoteObject, SendRequest(_, _, _, _)).Times(1).WillRepeatedly(DoAll(Return(NO_ERROR)));
    std::shared_ptr<AnsDistributedProxy> proxy = std::make_shared<AnsDistributedProxy>(iremoteObject);
    ASSERT_NE(nullptr, proxy);
    
    bool isUsing = false;
    ErrCode res = proxy->CheckRemoteDevicesIsUsing(isUsing);
    EXPECT_EQ(res, ERR_OK);
}

/*
 * @tc.name: CheckRemoteDevicesIsUsing_0200
 * @tc.desc: test AnsDistributedProxy's CheckRemoteDevicesIsUsing function
 * @tc.type: FUNC
 */
HWTEST_F(AnsDistributedProxyUnitTest, CheckRemoteDevicesIsUsing_0200, Function | MediumTest | Level1)
{
    GTEST_LOG_(INFO)
        << "AnsDistributedProxyUnitTest, CheckRemoteDevicesIsUsing_0200, TestSize.Level1";
    sptr<MockIRemoteObject> iremoteObject = new (std::nothrow) MockIRemoteObject();
    ASSERT_NE(nullptr, iremoteObject);
    EXPECT_CALL(*iremoteObject, SendRequest(_, _, _, _)).Times(1).WillRepeatedly(DoAll(Return(DEAD_OBJECT)));
    std::shared_ptr<AnsDistributedProxy> proxy = std::make_shared<AnsDistributedProxy>(iremoteObject);
    ASSERT_NE(nullptr, proxy);
    
    bool isUsing = false;
    ErrCode res = proxy->CheckRemoteDevicesIsUsing(isUsing);
    EXPECT_EQ(res, ERR_DEAD_OBJECT);
}

/*
 * @tc.name: SetLocalScreenStatus_0100
 * @tc.desc: test AnsDistributedProxy's SetLocalScreenStatus function
 * @tc.type: FUNC
 */
HWTEST_F(AnsDistributedProxyUnitTest, SetLocalScreenStatus_0100, Function | MediumTest | Level1)
{
    GTEST_LOG_(INFO)
        << "AnsDistributedProxyUnitTest, SetLocalScreenStatus_0100, TestSize.Level1";
    sptr<MockIRemoteObject> iremoteObject = new (std::nothrow) MockIRemoteObject();
    ASSERT_NE(nullptr, iremoteObject);
    EXPECT_CALL(*iremoteObject, SendRequest(_, _, _, _)).Times(1).WillRepeatedly(DoAll(Return(NO_ERROR)));
    std::shared_ptr<AnsDistributedProxy> proxy = std::make_shared<AnsDistributedProxy>(iremoteObject);
    ASSERT_NE(nullptr, proxy);
    
    bool isUsing = false;
    ErrCode res = proxy->SetLocalScreenStatus(isUsing);
    EXPECT_EQ(res, ERR_OK);
}

/*
 * @tc.name: SetLocalScreenStatus_0200
 * @tc.desc: test AnsDistributedProxy's SetLocalScreenStatus function
 * @tc.type: FUNC
 */
HWTEST_F(AnsDistributedProxyUnitTest, SetLocalScreenStatus_0200, Function | MediumTest | Level1)
{
    GTEST_LOG_(INFO)
        << "AnsDistributedProxyUnitTest, SetLocalScreenStatus_0200, TestSize.Level1";
    sptr<MockIRemoteObject> iremoteObject = new (std::nothrow) MockIRemoteObject();
    ASSERT_NE(nullptr, iremoteObject);
    EXPECT_CALL(*iremoteObject, SendRequest(_, _, _, _)).Times(1).WillRepeatedly(DoAll(Return(DEAD_OBJECT)));
    std::shared_ptr<AnsDistributedProxy> proxy = std::make_shared<AnsDistributedProxy>(iremoteObject);
    ASSERT_NE(nullptr, proxy);
    
    bool isUsing = false;
    ErrCode res = proxy->SetLocalScreenStatus(isUsing);
    EXPECT_EQ(res, ERR_DEAD_OBJECT);
}

/*
 * @tc.name: ResetFfrtQueue_0100
 * @tc.desc: test AnsDistributedProxy's ResetFfrtQueue function
 * @tc.type: FUNC
 */
HWTEST_F(AnsDistributedProxyUnitTest, ResetFfrtQueue_0100, Function | MediumTest | Level1)
{
    GTEST_LOG_(INFO)
        << "AnsDistributedProxyUnitTest, ResetFfrtQueue_0100, TestSize.Level1";
    sptr<MockIRemoteObject> iremoteObject = new (std::nothrow) MockIRemoteObject();
    ASSERT_NE(nullptr, iremoteObject);
    EXPECT_CALL(*iremoteObject, SendRequest(_, _, _, _)).Times(1).WillRepeatedly(DoAll(Return(NO_ERROR)));
    std::shared_ptr<AnsDistributedProxy> proxy = std::make_shared<AnsDistributedProxy>(iremoteObject);
    ASSERT_NE(nullptr, proxy);
    
    ErrCode res = proxy->ResetFfrtQueue();
    EXPECT_EQ(res, ERR_OK);
}

/*
 * @tc.name: ResetFfrtQueue_0200
 * @tc.desc: test AnsDistributedProxy's ResetFfrtQueue function
 * @tc.type: FUNC
 */
HWTEST_F(AnsDistributedProxyUnitTest, ResetFfrtQueue_0200, Function | MediumTest | Level1)
{
    GTEST_LOG_(INFO)
        << "AnsDistributedProxyUnitTest, ResetFfrtQueue_0200, TestSize.Level1";
    sptr<MockIRemoteObject> iremoteObject = new (std::nothrow) MockIRemoteObject();
    ASSERT_NE(nullptr, iremoteObject);
    EXPECT_CALL(*iremoteObject, SendRequest(_, _, _, _)).Times(1).WillRepeatedly(DoAll(Return(DEAD_OBJECT)));
    std::shared_ptr<AnsDistributedProxy> proxy = std::make_shared<AnsDistributedProxy>(iremoteObject);
    ASSERT_NE(nullptr, proxy);
    
    ErrCode res = proxy->ResetFfrtQueue();
    EXPECT_EQ(res, ERR_DEAD_OBJECT);
}