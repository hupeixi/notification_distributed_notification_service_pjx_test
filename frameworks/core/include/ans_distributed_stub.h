/*
 * Copyright (c) 2021-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef BASE_NOTIFICATION_ANS_STANDARD_FRAMEWORKS_ANS_CORE_INCLUDE_DNS_DISTRIBUTED_STUB_H
#define BASE_NOTIFICATION_ANS_STANDARD_FRAMEWORKS_ANS_CORE_INCLUDE_DNS_DISTRIBUTED_STUB_H

#include <functional>
#include <map>

#include "ans_distributed_interface.h"
#include "distributed_notification_service_ipc_interface_code.h"
#include "iremote_stub.h"

namespace OHOS {
namespace Notification {
class AnsDistributedStub : public IRemoteStub<AnsDistributedInterface> {
public:
    AnsDistributedStub();
    ~AnsDistributedStub() override;
    DISALLOW_COPY_AND_MOVE(AnsDistributedStub);

    /**
     * @brief Handle remote request.
     *
     * @param data Indicates the input parcel.
     * @param reply Indicates the output parcel.
     * @param option Indicates the message option.
     * @return Returns ERR_OK on success, others on failure.
     */
    virtual int OnRemoteRequest(
        uint32_t code, MessageParcel &data, MessageParcel &reply, MessageOption &option) override;

    /**
     * @brief Obtains the death event of the Distributed KvStore service.
     */
    ErrCode OnDistributedKvStoreDeathRecipient() override;

    /**
     * @brief Removes a local notification.
     *
     * @param bundleName Indicates the bundle name of the application whose notifications are to be remove.
     * @param label Indicates the label of the notifications.
     * @param id Indicates the bundle uid of the application whose notifications are to be remove.
     * @return ErrCode Returns the remove result.
     */
    ErrCode Delete(const std::string &bundleName, const std::string &label, int32_t id) override;

    /**
     * @brief Removes a remote notification.
     *
     * @param deviceId Indicates the ID of the device.
     * @param bundleName Indicates the bundle name of the application whose notifications are to be remove.
     * @param label Indicates the label of the notifications.
     * @param id Indicates the bundle uid of the application whose notifications are to be remove.
     * @return ErrCode Returns the remove result.
     */
    ErrCode DeleteRemoteNotification(
        const std::string &deviceId, const std::string &bundleName, const std::string &label, int32_t id) override;

    /**
     * @brief Publishes a local notification to remote device.
     *
     * @param bundleName Indicates the bundle name of the application whose notifications are to be publish.
     * @param label Indicates the label of the notifications.
     * @param id Indicates the bundle uid of the application whose notifications are to be publish.
     * @param request Indicates the NotificationRequest object for setting the notification content.
     * @return ErrCode Returns the publish result.
     */    
    ErrCode Publish(
        const std::string &bundleName, const std::string &label, int32_t id, const sptr<NotificationRequest> &request) override;

    /**
     * @brief Check if any other device screen is on.
     *
     * @param isUsing True for any other device screen is on, otherwise false.
     * @return Returns the error code.
     */
    ErrCode CheckRemoteDevicesIsUsing(bool &isUsing) override;

    /**
     * @brief Set screen status of local device.
     *
     * @param screenOn Indicates the local device screen status.
     * @return Returns the error code.
     */
    ErrCode SetLocalScreenStatus(bool screenOn) override;

    /**
     * @brief Reset ffrt queue
     */
    ErrCode ResetFfrtQueue() override;
private:
    static const std::map<AnsDistributedInterfaceCode, std::function<ErrCode(AnsDistributedStub *, MessageParcel &, MessageParcel &)>>
        interfaces_;

    ErrCode HandleOnDistributedKvStoreDeathRecipient(MessageParcel &data, MessageParcel &reply);
    ErrCode HandleDelete(MessageParcel &data, MessageParcel &reply);
    ErrCode HandleDeleteRemoteNotification(MessageParcel &data, MessageParcel &reply);
    ErrCode HandlePublish(MessageParcel &data, MessageParcel &reply);
    ErrCode HandleCheckRemoteDevicesIsUsing(MessageParcel &data, MessageParcel &reply);
    ErrCode HandleSetLocalScreenStatus(MessageParcel &data, MessageParcel &reply);
    ErrCode HandleResetFfrtQueue(MessageParcel &data, MessageParcel &reply);
};
}  // namespace Notification
}  // namespace OHOS

#endif  // BASE_NOTIFICATION_ANS_STANDARD_FRAMEWORKS_ANS_CORE_INCLUDE_DNS_MANAGER_STUB_H
