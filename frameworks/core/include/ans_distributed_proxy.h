/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef BASE_NOTIFICATION_ANS_STANDARD_FRAMEWORKS_ANS_CORE_INCLUDE_DNS_DISTRIBUTED_PROXY_H
#define BASE_NOTIFICATION_ANS_STANDARD_FRAMEWORKS_ANS_CORE_INCLUDE_DNS_DISTRIBUTED_PROXY_H


#include <string>
#include <vector>


#include "ans_distributed_interface.h"
#include "distributed_notification_service_ipc_interface_code.h"
#include "notification_bundle_option.h"
#include "iremote_proxy.h"

namespace OHOS::Notification {
class AnsDistributedProxy : public IRemoteProxy<AnsDistributedInterface> {
public:
    explicit AnsDistributedProxy(const sptr<IRemoteObject> &impl)
        : IRemoteProxy<AnsDistributedInterface>(impl) {}
    ~AnsDistributedProxy() override = default;
    DISALLOW_COPY_AND_MOVE(AnsDistributedProxy);

    /**
     * @brief Obtains the death event of the Distributed KvStore service.
     */
    ErrCode OnDistributedKvStoreDeathRecipient() override;

    /**
     * @brief Removes a local notification.
     *
     * @param bundleName Indicates the bundle name of the application whose notifications are to be remove.
     * @param label Indicates the label of the notifications.
     * @param id Indicates the bundle uid of the application whose notifications are to be remove.
     * @return ErrCode Returns the remove result.
     */
    ErrCode Delete(const std::string &bundleName, const std::string &label, int32_t id) override;

    /**
     * @brief Removes a remote notification.
     *
     * @param deviceId Indicates the ID of the device.
     * @param bundleName Indicates the bundle name of the application whose notifications are to be remove.
     * @param label Indicates the label of the notifications.
     * @param id Indicates the bundle uid of the application whose notifications are to be remove.
     * @return ErrCode Returns the remove result.
     */
    ErrCode DeleteRemoteNotification(
        const std::string &deviceId, const std::string &bundleName, const std::string &label, int32_t id) override;

    /**
     * @brief Publishes a local notification to remote device.
     *
     * @param bundleName Indicates the bundle name of the application whose notifications are to be publish.
     * @param label Indicates the label of the notifications.
     * @param id Indicates the bundle uid of the application whose notifications are to be publish.
     * @param request Indicates the NotificationRequest object for setting the notification content.
     * @return ErrCode Returns the publish result.
     */    
    ErrCode Publish(
        const std::string &bundleName, const std::string &label, int32_t id, const sptr<NotificationRequest> &request) override;

    /**
     * @brief Check if any other device screen is on.
     *
     * @param isUsing True for any other device screen is on, otherwise false.
     * @return Returns the error code.
     */
    ErrCode CheckRemoteDevicesIsUsing(bool &isUsing) override;

    /**
     * @brief Set screen status of local device.
     *
     * @param screenOn Indicates the local device screen status.
     * @return Returns the error code.
     */
    ErrCode SetLocalScreenStatus(bool screenOn) override;

    /**
     * @brief Reset ffrt queue
     */
    ErrCode ResetFfrtQueue() override;
private:
    ErrCode InnerTransact(AnsDistributedInterfaceCode code, 
        MessageOption &flags, MessageParcel &data, MessageParcel &reply);

    static inline BrokerDelegator<AnsDistributedProxy> delegator_;
};
} // namespace OHOS::Notification

#endif // BASE_NOTIFICATION_ANS_DIALOG_CALLBACK_PROXY_H
