# Copyright (c) 2022-2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//base/notification/distributed_notification_service/notification.gni")
import("//build/ohos.gni")
import("//build/test.gni")

module_output_path = "${component_name}/unittest"

ohos_unittest("reminder_unit_test") {
  module_out_path = module_output_path
  include_dirs = [
    ".",
    "include",
    "/${services_path}/ans/include",
    "${services_path}/ans/test/unittest/mock/include",
  ]

  defines = []

  sources = [
    "mock/blob.cpp",
    "mock/distributed_kv_data_manager.cpp",
    "mock/mock_access_token_helper.cpp",
    "mock/mock_bundle_manager_helper.cpp",
    "mock/mock_event_handler.cpp",
    "mock/mock_ipc.cpp",
    "mock/mock_single_kv_store.cpp",
    "reminder_data_manager_test.cpp",
  ]

  deps = [
    "${frameworks_module_ans_path}:ans_innerkits",
    "${services_path}/ans:libans",
    "//third_party/googletest:gtest_main",
  ]

  if (distributed_notification_supported) {
    defines += [ "DISTRIBUTED_NOTIFICATION_SUPPORTED" ]
    deps += [ "${services_path}/distributed:libans_distributed" ]
    include_dirs += [ "${services_path}/distributed/include" ]
  }

  external_deps = [
    "ability_base:want",
    "ability_base:zuri",
    "ability_runtime:ability_manager",
    "ability_runtime:abilitykit_native",
    "ability_runtime:app_manager",
    "ability_runtime:wantagent_innerkits",
    "access_token:libaccesstoken_sdk",
    "bundle_framework:appexecfwk_base",
    "bundle_framework:appexecfwk_core",
    "c_utils:utils",
    "common_event_service:cesfwk_innerkits",
    "data_share:datashare_common",
    "data_share:datashare_consumer",
    "device_manager:devicemanagersdk",
    "eventhandler:libeventhandler",
    "ffrt:libffrt",
    "hilog:libhilog",
    "hitrace:hitrace_meter",
    "hitrace:libhitracechain",
    "image_framework:image_native",
    "ipc:ipc_core",
    "kv_store:distributeddata_inner",
    "os_account:os_account_innerkits",
    "player_framework:media_client",
    "relational_store:native_rdb",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
    "time_service:time_client",
  ]

  if (device_usage) {
    external_deps += [ "device_usage_statistics:usagestatsinner" ]
    defines += [ "DEVICE_USAGE_STATISTICS_ENABLE" ]
  }

  cflags = []
  if (hisysevent_usage) {
    external_deps += [ "hisysevent:libhisysevent" ]
    cflags += [ "-DHAS_HISYSEVENT_PART" ]
  }

  subsystem_name = "${subsystem_name}"
  part_name = "${component_name}"
}

ohos_unittest("ans_unit_test") {
  module_out_path = module_output_path
  include_dirs = [
    ".",
    "include",
    "/${services_path}/ans/include",
    "${services_path}/ans/test/unittest/mock/include",
    "${services_path}/../framworks/core/include",
  ]

  defines = []

  sources = [
    "${test_path}/mock/mock_tokenid_kit.cpp",
    "bundle_manager_helper_test.cpp",
    "mock/blob.cpp",
    "mock/distributed_kv_data_manager.cpp",
    "mock/mock_access_token_helper.cpp",
    "mock/mock_accesstoken_kit.cpp",
    "mock/mock_bundle_manager_helper.cpp",
    "mock/mock_event_handler.cpp",
    "mock/mock_ipc.cpp",
    "mock/mock_push_callback_stub.cpp",
    "mock/mock_single_kv_store.cpp",
    "notification_hisysevent_test.cpp",
    "notification_local_live_view_subscriber_manager_test.cpp",
    "notification_preferences_database_test.cpp",
    "notification_slot_filter_test.cpp",
    "notification_subscriber_manager_test.cpp",
    "permission_filter_test.cpp",
    "system_event_observer_test.cpp",
  ]

  deps = [
    "${frameworks_module_ans_path}:ans_innerkits",
    "${services_path}/ans:libans",
    "//third_party/googletest:gmock_main",
    "//third_party/googletest:gtest_main",
  ]

  if (distributed_notification_supported) {
    defines += [ "DISTRIBUTED_NOTIFICATION_SUPPORTED" ]
    deps += [ "${services_path}/distributed:libans_distributed" ]
    include_dirs += [ "${services_path}/distributed/include" ]
  }

  external_deps = [
    "ability_base:want",
    "ability_base:zuri",
    "ability_runtime:abilitykit_native",
    "ability_runtime:app_manager",
    "ability_runtime:wantagent_innerkits",
    "access_token:libaccesstoken_sdk",
    "bundle_framework:appexecfwk_base",
    "bundle_framework:appexecfwk_core",
    "c_utils:utils",
    "common_event_service:cesfwk_innerkits",
    "device_manager:devicemanagersdk",
    "eventhandler:libeventhandler",
    "ffrt:libffrt",
    "hilog:libhilog",
    "hitrace:hitrace_meter",
    "hitrace:libhitracechain",
    "image_framework:image_native",
    "ipc:ipc_core",
    "kv_store:distributeddata_inner",
    "os_account:os_account_innerkits",
    "player_framework:media_client",
    "relational_store:native_rdb",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
    "time_service:time_client",
  ]

  if (device_usage) {
    external_deps += [ "device_usage_statistics:usagestatsinner" ]
    defines += [ "DEVICE_USAGE_STATISTICS_ENABLE" ]
  }

  cflags = []
  if (hisysevent_usage) {
    external_deps += [ "hisysevent:libhisysevent" ]
    cflags += [ "-DHAS_HISYSEVENT_PART" ]
  }

  subsystem_name = "${subsystem_name}"
  part_name = "${component_name}"
}

ohos_unittest("notification_service_test") {
  module_out_path = module_output_path
  include_dirs = [
    ".",
    "include",
    "/${services_path}/ans/include",
    "${services_path}/ans/test/unittest/mock/include",
  ]

  defines = []

  sources = [
    "${test_path}/mock/mock_tokenid_kit.cpp",
    "advanced_notification_service_test.cpp",
    "mock/blob.cpp",
    "mock/distributed_kv_data_manager.cpp",
    "mock/mock_access_token_helper.cpp",
    "mock/mock_accesstoken_kit.cpp",
    "mock/mock_bundle_manager_helper.cpp",
    "mock/mock_bundle_mgr.cpp",
    "mock/mock_event_handler.cpp",
    "mock/mock_ipc.cpp",
    "mock/mock_push_callback_stub.cpp",
    "mock/mock_single_kv_store.cpp",
    "notification_dialog_test/mock_os_account_manager_annex.cpp",
  ]

  deps = [
    "${frameworks_module_ans_path}:ans_innerkits",
    "${services_path}/ans:libans",
    "//third_party/googletest:gtest_main",
  ]

  if (distributed_notification_supported) {
    defines += [ "DISTRIBUTED_NOTIFICATION_SUPPORTED" ]
    deps += [ "${services_path}/distributed:libans_distributed" ]
    include_dirs += [ "${services_path}/distributed/include" ]
  }

  external_deps = [
    "ability_base:want",
    "ability_base:zuri",
    "ability_runtime:abilitykit_native",
    "ability_runtime:app_manager",
    "ability_runtime:wantagent_innerkits",
    "access_token:libaccesstoken_sdk",
    "bundle_framework:appexecfwk_base",
    "bundle_framework:appexecfwk_core",
    "c_utils:utils",
    "common_event_service:cesfwk_innerkits",
    "device_manager:devicemanagersdk",
    "eventhandler:libeventhandler",
    "ffrt:libffrt",
    "hilog:libhilog",
    "hitrace:hitrace_meter",
    "hitrace:libhitracechain",
    "image_framework:image_native",
    "ipc:ipc_core",
    "kv_store:distributeddata_inner",
    "os_account:os_account_innerkits",
    "player_framework:media_client",
    "relational_store:native_rdb",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
    "time_service:time_client",
  ]

  if (device_usage) {
    external_deps += [ "device_usage_statistics:usagestatsinner" ]
    defines += [ "DEVICE_USAGE_STATISTICS_ENABLE" ]
  }

  cflags = []
  if (hisysevent_usage) {
    external_deps += [ "hisysevent:libhisysevent" ]
    cflags += [ "-DHAS_HISYSEVENT_PART" ]
  }

  subsystem_name = "${subsystem_name}"
  part_name = "${component_name}"
}

ohos_unittest("notification_preferences_test") {
  module_out_path = module_output_path
  include_dirs = [
    ".",
    "include",
    "/${services_path}/ans/include",
    "${services_path}/ans/test/unittest/mock/include",
  ]

  defines = []

  sources = [
    "${test_path}/mock/mock_tokenid_kit.cpp",
    "advanced_notification_service_ability_test.cpp",
    "mock/blob.cpp",
    "mock/distributed_kv_data_manager.cpp",
    "mock/mock_access_token_helper.cpp",
    "mock/mock_accesstoken_kit.cpp",
    "mock/mock_bundle_manager_helper.cpp",
    "mock/mock_event_handler.cpp",
    "mock/mock_ipc.cpp",
    "mock/mock_push_callback_stub.cpp",
    "mock/mock_single_kv_store.cpp",
    "notification_preferences_test.cpp",
  ]

  deps = [
    "${frameworks_module_ans_path}:ans_innerkits",
    "${services_path}/ans:libans",
    "//third_party/googletest:gtest_main",
  ]

  if (distributed_notification_supported) {
    defines += [ "DISTRIBUTED_NOTIFICATION_SUPPORTED" ]
    deps += [ "${services_path}/distributed:libans_distributed" ]
    include_dirs += [ "${services_path}/distributed/include" ]
  }

  external_deps = [
    "ability_base:want",
    "ability_base:zuri",
    "ability_runtime:abilitykit_native",
    "ability_runtime:app_manager",
    "ability_runtime:wantagent_innerkits",
    "access_token:libaccesstoken_sdk",
    "bundle_framework:appexecfwk_base",
    "bundle_framework:appexecfwk_core",
    "c_utils:utils",
    "common_event_service:cesfwk_innerkits",
    "data_share:datashare_common",
    "data_share:datashare_consumer",
    "device_manager:devicemanagersdk",
    "eventhandler:libeventhandler",
    "ffrt:libffrt",
    "hilog:libhilog",
    "hitrace:hitrace_meter",
    "hitrace:libhitracechain",
    "image_framework:image_native",
    "ipc:ipc_core",
    "kv_store:distributeddata_inner",
    "os_account:os_account_innerkits",
    "player_framework:media_client",
    "relational_store:native_rdb",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
    "time_service:time_client",
  ]

  if (device_usage) {
    external_deps += [ "device_usage_statistics:usagestatsinner" ]
    defines += [ "DEVICE_USAGE_STATISTICS_ENABLE" ]
  }

  cflags = []
  if (hisysevent_usage) {
    external_deps += [ "hisysevent:libhisysevent" ]
    cflags += [ "-DHAS_HISYSEVENT_PART" ]
  }

  subsystem_name = "${subsystem_name}"
  part_name = "${component_name}"
}

ohos_unittest("access_token_helper_test") {
  module_out_path = module_output_path
  include_dirs = [
    ".",
    "include",
    "/${services_path}/ans/include",
    "${services_path}/ans/test/unittest/mock/include",
  ]

  sources = [
    "${services_path}/ans/src/access_token_helper.cpp",
    "${services_path}/ans/test/unittest/mock/mock_accesstoken_kit.cpp",
    "${test_path}/mock/mock_tokenid_kit.cpp",
    "access_token_helper_test/access_token_helper_test.cpp",
  ]

  deps = [ "${frameworks_module_ans_path}:ans_innerkits" ]

  external_deps = [
    "ability_base:want",
    "ability_base:zuri",
    "ability_runtime:abilitykit_native",
    "ability_runtime:app_manager",
    "ability_runtime:wantagent_innerkits",
    "access_token:libaccesstoken_sdk",
    "access_token:libtokenid_sdk",
    "bundle_framework:appexecfwk_base",
    "bundle_framework:appexecfwk_core",
    "c_utils:utils",
    "common_event_service:cesfwk_innerkits",
    "eventhandler:libeventhandler",
    "hilog:libhilog",
    "hitrace:hitrace_meter",
    "hitrace:libhitracechain",
    "image_framework:image_native",
    "ipc:ipc_core",
    "kv_store:distributeddata_inner",
    "os_account:os_account_innerkits",
    "player_framework:media_client",
    "relational_store:native_rdb",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
    "time_service:time_client",
  ]

  subsystem_name = "${subsystem_name}"
  part_name = "${component_name}"
}

ohos_unittest("bundle_manager_helper_branch_test") {
  module_out_path = module_output_path
  include_dirs = [
    ".",
    "include",
    "/${services_path}/ans/include",
    "${services_path}/ans/test/unittest/mock/include",
  ]

  sources = [
    "bundle_manager_helper_branch_test/bundle_manager_helper_branch_test.cpp",
    "bundle_manager_helper_branch_test/mock_service_registry.cpp",
  ]

  deps = [
    "${frameworks_module_ans_path}:ans_innerkits",
    "${services_path}/ans:libans",
    "//third_party/googletest:gtest_main",
  ]

  external_deps = [
    "ability_base:want",
    "ability_base:zuri",
    "ability_runtime:abilitykit_native",
    "ability_runtime:app_manager",
    "ability_runtime:wantagent_innerkits",
    "access_token:libaccesstoken_sdk",
    "bundle_framework:appexecfwk_base",
    "bundle_framework:appexecfwk_core",
    "c_utils:utils",
    "common_event_service:cesfwk_innerkits",
    "eventhandler:libeventhandler",
    "hilog:libhilog",
    "hitrace:hitrace_meter",
    "hitrace:libhitracechain",
    "image_framework:image_native",
    "ipc:ipc_core",
    "kv_store:distributeddata_inner",
    "os_account:os_account_innerkits",
    "player_framework:media_client",
    "relational_store:native_rdb",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
    "time_service:time_client",
  ]

  subsystem_name = "${subsystem_name}"
  part_name = "${component_name}"
}

ohos_unittest("notification_dialog_test") {
  module_out_path = module_output_path
  include_dirs = [
    ".",
    "include",
    "/${services_path}/ans/include",
    "${services_path}/ans/test/unittest/mock/include",
  ]

  sources = [
    "notification_dialog_test/mock_os_account_manager_annex.cpp",
    "notification_dialog_test/notification_dialog_test.cpp",
  ]

  deps = [
    "${frameworks_module_ans_path}:ans_innerkits",
    "${services_path}/ans:libans",
    "//third_party/googletest:gtest_main",
  ]

  external_deps = [
    "ability_base:want",
    "ability_base:zuri",
    "ability_runtime:abilitykit_native",
    "ability_runtime:app_manager",
    "ability_runtime:wantagent_innerkits",
    "access_token:libaccesstoken_sdk",
    "bundle_framework:appexecfwk_base",
    "bundle_framework:appexecfwk_core",
    "c_utils:utils",
    "common_event_service:cesfwk_innerkits",
    "eventhandler:libeventhandler",
    "hilog:libhilog",
    "hitrace:hitrace_meter",
    "hitrace:libhitracechain",
    "image_framework:image_native",
    "ipc:ipc_core",
    "kv_store:distributeddata_inner",
    "os_account:os_account_innerkits",
    "player_framework:media_client",
    "relational_store:native_rdb",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
    "time_service:time_client",
  ]

  subsystem_name = "${subsystem_name}"
  part_name = "${component_name}"
}

ohos_unittest("notification_subscriber_manager_branch_test") {
  module_out_path = module_output_path
  include_dirs = [
    ".",
    "include",
    "/${services_path}/ans/include",
    "${services_path}/ans/test/unittest/mock/include",
    "${ffrt_path}/interfaces/kits",
  ]

  defines = []

  sources = [
    "${test_path}/mock/mock_tokenid_kit.cpp",
    "mock/mock_accesstoken_kit.cpp",
    "mock/mock_bundle_manager_helper.cpp",
    "mock/mock_ipc.cpp",
    "notification_subscriber_manager_branch_test/mock_access_token_helper.cpp",
    "notification_subscriber_manager_branch_test/mock_notification.cpp",
    "notification_subscriber_manager_branch_test/mock_notification_preferences.cpp",
    "notification_subscriber_manager_branch_test/mock_os_account_manager.cpp",
    "notification_subscriber_manager_branch_test/notification_subscriber_manager_branch_test.cpp",
  ]

  deps = [
    "${frameworks_module_ans_path}:ans_innerkits",
    "${services_path}/ans:libans",
    "//third_party/googletest:gtest_main",
  ]

  if (distributed_notification_supported) {
    defines += [ "DISTRIBUTED_NOTIFICATION_SUPPORTED" ]
    deps += [ "${services_path}/distributed:libans_distributed" ]
    include_dirs += [ "${services_path}/distributed/include" ]
  }

  external_deps = [
    "ability_base:want",
    "ability_base:zuri",
    "ability_runtime:abilitykit_native",
    "ability_runtime:app_manager",
    "ability_runtime:wantagent_innerkits",
    "access_token:libaccesstoken_sdk",
    "bundle_framework:appexecfwk_base",
    "bundle_framework:appexecfwk_core",
    "c_utils:utils",
    "common_event_service:cesfwk_innerkits",
    "eventhandler:libeventhandler",
    "ffrt:libffrt",
    "hilog:libhilog",
    "hitrace:hitrace_meter",
    "hitrace:libhitracechain",
    "image_framework:image_native",
    "ipc:ipc_core",
    "kv_store:distributeddata_inner",
    "os_account:os_account_innerkits",
    "player_framework:media_client",
    "relational_store:native_rdb",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
    "time_service:time_client",
  ]

  subsystem_name = "${subsystem_name}"
  part_name = "${component_name}"
}

ohos_unittest("advanced_notification_service_branch_test") {
  module_out_path = module_output_path
  include_dirs = [
    ".",
    "include",
    "/${services_path}/ans/include",
    "${services_path}/ans/test/unittest/mock/include",
    "${ffrt_path}/interfaces/kits",
  ]

  defines = []

  sources = [
    "${test_path}/mock/mock_tokenid_kit.cpp",
    "advanced_notification_service_branch_test.cpp",
    "mock/blob.cpp",
    "mock/distributed_kv_data_manager.cpp",
    "mock/mock_accesstoken_kit.cpp",
    "mock/mock_advanced_notification_service.cpp",
    "mock/mock_bundle_manager_helper.cpp",
    "mock/mock_event_handler.cpp",
    "mock/mock_ipc.cpp",
    "mock/mock_single_kv_store.cpp",
    "notification_dialog_test/mock_os_account_manager_annex.cpp",
    "notification_subscriber_manager_branch_test/mock_access_token_helper.cpp",
  ]

  deps = [
    "${frameworks_module_ans_path}:ans_innerkits",
    "${services_path}/ans:libans",
    "//third_party/googletest:gtest_main",
  ]

  if (distributed_notification_supported) {
    defines += [ "DISTRIBUTED_NOTIFICATION_SUPPORTED" ]
    deps += [ "${services_path}/distributed:libans_distributed" ]
    include_dirs += [ "${services_path}/distributed/include" ]
  }

  external_deps = [
    "ability_base:want",
    "ability_base:zuri",
    "ability_runtime:abilitykit_native",
    "ability_runtime:app_manager",
    "ability_runtime:wantagent_innerkits",
    "access_token:libaccesstoken_sdk",
    "bundle_framework:appexecfwk_base",
    "bundle_framework:appexecfwk_core",
    "c_utils:utils",
    "common_event_service:cesfwk_innerkits",
    "eventhandler:libeventhandler",
    "ffrt:libffrt",
    "hilog:libhilog",
    "hitrace:hitrace_meter",
    "hitrace:libhitracechain",
    "image_framework:image_native",
    "ipc:ipc_core",
    "kv_store:distributeddata_inner",
    "os_account:os_account_innerkits",
    "player_framework:media_client",
    "relational_store:native_rdb",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
    "time_service:time_client",
  ]

  subsystem_name = "${subsystem_name}"
  part_name = "${component_name}"
}

ohos_unittest("notification_preferences_database_branch_test") {
  module_out_path = module_output_path
  include_dirs = [
    ".",
    "include",
    "/${services_path}/ans/include",
    "${services_path}/ans/test/unittest/mock/include",
    "${ffrt_path}/interfaces/kits",
  ]

  sources = [
    "${services_path}/ans/src/notification_config_parse.cpp",
    "${services_path}/ans/src/notification_preferences_database.cpp",
    "${services_path}/ans/src/notification_preferences_info.cpp",
    "notification_preferences_database_branch_test/mock_notification_rdb_data_mgr.cpp",
    "notification_preferences_database_branch_test/notification_preferences_database_branch_test.cpp",
  ]

  deps = [
    "${frameworks_module_ans_path}:ans_innerkits",
    "//third_party/googletest:gtest_main",
    "//third_party/libxml2:libxml2",
  ]

  external_deps = [
    "ability_base:want",
    "ability_base:zuri",
    "ability_runtime:abilitykit_native",
    "ability_runtime:app_manager",
    "ability_runtime:wantagent_innerkits",
    "access_token:libaccesstoken_sdk",
    "bundle_framework:appexecfwk_base",
    "bundle_framework:appexecfwk_core",
    "c_utils:utils",
    "common_event_service:cesfwk_innerkits",
    "eventhandler:libeventhandler",
    "hilog:libhilog",
    "hitrace:hitrace_meter",
    "hitrace:libhitracechain",
    "image_framework:image_native",
    "ipc:ipc_core",
    "kv_store:distributeddata_inner",
    "os_account:os_account_innerkits",
    "player_framework:media_client",
    "relational_store:native_rdb",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
    "time_service:time_client",
  ]

  subsystem_name = "${subsystem_name}"
  part_name = "${component_name}"
}

ohos_unittest("push_callback_stub_test") {
  module_out_path = module_output_path
  include_dirs = [
    ".",
    "include",
    "/${services_path}/ans/include",
  ]

  sources = [ "push_callback_stub_test.cpp" ]

  deps = [
    "${frameworks_module_ans_path}:ans_innerkits",
    "${services_path}/ans:libans",
    "//third_party/googletest:gtest_main",
  ]

  external_deps = [
    "ability_base:want",
    "ability_base:zuri",
    "ability_runtime:abilitykit_native",
    "ability_runtime:app_manager",
    "ability_runtime:wantagent_innerkits",
    "access_token:libaccesstoken_sdk",
    "bundle_framework:appexecfwk_base",
    "bundle_framework:appexecfwk_core",
    "c_utils:utils",
    "common_event_service:cesfwk_innerkits",
    "eventhandler:libeventhandler",
    "hilog:libhilog",
    "hitrace:hitrace_meter",
    "hitrace:libhitracechain",
    "image_framework:image_native",
    "ipc:ipc_core",
    "kv_store:distributeddata_inner",
    "os_account:os_account_innerkits",
    "player_framework:media_client",
    "relational_store:native_rdb",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
    "time_service:time_client",
  ]

  subsystem_name = "${subsystem_name}"
  part_name = "${component_name}"
}

ohos_unittest("notification_rdb_data_mgr_test") {
  module_out_path = module_output_path
  include_dirs = [
    ".",
    "include",
    "/${services_path}/ans/include",
    "${services_path}/ans/test/unittest/mock/include",
  ]

  sources = [
    "notification_rdb_data_mgr_test/mock_abs_result_set.cpp",
    "notification_rdb_data_mgr_test/mock_abs_shared_result_set.cpp",
    "notification_rdb_data_mgr_test/mock_rdb_helper.cpp",
    "notification_rdb_data_mgr_test/notification_rdb_data_mgr_test.cpp",
  ]

  deps = [
    "${frameworks_module_ans_path}:ans_innerkits",
    "${services_path}/ans:libans",
    "//third_party/googletest:gtest_main",
  ]

  external_deps = [
    "ability_base:want",
    "ability_base:zuri",
    "ability_runtime:abilitykit_native",
    "ability_runtime:app_manager",
    "ability_runtime:wantagent_innerkits",
    "access_token:libaccesstoken_sdk",
    "bundle_framework:appexecfwk_base",
    "bundle_framework:appexecfwk_core",
    "c_utils:utils",
    "common_event_service:cesfwk_innerkits",
    "eventhandler:libeventhandler",
    "hilog:libhilog",
    "hitrace:hitrace_meter",
    "hitrace:libhitracechain",
    "image_framework:image_native",
    "ipc:ipc_core",
    "kv_store:distributeddata_inner",
    "os_account:os_account_innerkits",
    "player_framework:media_client",
    "relational_store:native_rdb",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
    "time_service:time_client",
  ]

  subsystem_name = "${subsystem_name}"
  part_name = "${component_name}"
}

ohos_unittest("notification_config_parse_test") {
  module_out_path = module_output_path
  include_dirs = [
    ".",
    "include",
    "/${services_path}/ans/include",
    "${services_path}/ans/test/unittest/mock/include",
  ]

  sources = [ "notification_config_parse_test.cpp" ]

  deps = [
    "${frameworks_module_ans_path}:ans_innerkits",
    "${services_path}/ans:libans",
    "//third_party/googletest:gtest_main",
    "//third_party/libxml2:libxml2",
  ]

  external_deps = [
    "ability_base:want",
    "ability_base:zuri",
    "ability_runtime:abilitykit_native",
    "ability_runtime:app_manager",
    "ability_runtime:wantagent_innerkits",
    "access_token:libaccesstoken_sdk",
    "bundle_framework:appexecfwk_base",
    "bundle_framework:appexecfwk_core",
    "c_utils:utils",
    "common_event_service:cesfwk_innerkits",
    "eventhandler:libeventhandler",
    "hilog:libhilog",
    "hitrace:hitrace_meter",
    "hitrace:libhitracechain",
    "image_framework:image_native",
    "ipc:ipc_core",
    "kv_store:distributeddata_inner",
    "os_account:os_account_innerkits",
    "player_framework:media_client",
    "relational_store:native_rdb",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
    "time_service:time_client",
  ]
}

group("unittest") {
  testonly = true
  deps = [
    ":access_token_helper_test",
    ":advanced_notification_service_branch_test",
    ":ans_unit_test",
    ":bundle_manager_helper_branch_test",
    ":notification_config_parse_test",
    ":notification_dialog_test",
    ":notification_preferences_database_branch_test",
    ":notification_preferences_test",
    ":notification_rdb_data_mgr_test",
    ":notification_service_test",
    ":notification_subscriber_manager_branch_test",
    ":push_callback_stub_test",
    ":reminder_unit_test",
  ]
}
