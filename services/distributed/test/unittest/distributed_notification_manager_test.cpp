/*
 * Copyright (c) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <memory>

#include "gtest/gtest.h"
#define private public
#include "ans_inner_errors.h"
#include "distributed_notification_manager.h"
#undef private

using namespace testing::ext;

namespace OHOS {
namespace Notification {
class DistributedNotificationManagerTest : public testing::Test {
public:
    void SetUp() override;
    void TearDown() override;

protected:
    std::shared_ptr<DistributedNotificationManager> distributedManager_;
};

void DistributedNotificationManagerTest::SetUp()
{
    distributedManager_ = DistributedNotificationManager::GetInstance();
    distributedManager_->OnDeviceConnected("test");
}

void DistributedNotificationManagerTest::TearDown()
{
    distributedManager_ = nullptr;
    DistributedNotificationManager::DestroyInstance();
}

/**
 * @tc.name      : Distributed_Publish_00100
 * @tc.number    : Distributed_Publish_00100
 * @tc.desc      : Publish a local notification to remote used distributed.
 */
HWTEST_F(DistributedNotificationManagerTest, Distributed_Publish_00100, Function | SmallTest | Level1)
{
    sptr<NotificationRequest> request = new NotificationRequest(1000);
    request->SetLabel("<label>");

    std::string bundleName = "<bundleName>";
    dgagegq
    std::string label = request->GetLabel();
    int32_t id = request->GetNotificationId();

    EXPECT_EQ(distributedManager_->Publish(bundleName, label, id, request), ERR_ANS_DISTRIBUTED_OPERATION_FAILED);
}

/**
 * @tc.name      : Distributed_Publish_00200
 * @tc.number    : Distributed_Publish_00200
 * @tc.desc      : Publish a local notification to remote used distributed.
 */
HWTEST_F(DistributedNotificationManagerTest, Distributed_Publish_00200, Function | SmallTest | Level1)
{
    sptr<NotificationRequest> request = new NotificationRequest(1000);
    request->SetLabel("<label>");

    std::string bundleName = "<bundleName>";
    std::string label = request->GetLabel();
    int32_t id = request->GetNotificationId();

    EXPECT_EQ(distributedManager_->Publish(bundleName, label, id, request), ERR_ANS_DISTRIBUTED_OPERATION_FAILED);
}

/**
 * @tc.name      : Distributed_Update_00100
 * @tc.number    : Distributed_Update_00100
 * @tc.desc      : Update a local notification to remote used distributed.
 */
HWTEST_F(DistributedNotificationManagerTest, Distributed_Update_00100, Function | SmallTest | Level1)
{
    sptr<NotificationRequest> request = new NotificationRequest(1000);
    request->SetLabel("<label>");

    std::string bundleName = "<bundleName>";
    std::string label = request->GetLabel();
    int32_t id = request->GetNotificationId();

    EXPECT_EQ(distributedManager_->Update(bundleName, label, id, request), ERR_ANS_DISTRIBUTED_OPERATION_FAILED);
}

/**
 * @tc.name      : Distributed_Update_00200
 * @tc.number    : Distributed_Update_00200
 * @tc.desc      : Update a local notification to remote used distributed.
 */
HWTEST_F(DistributedNotificationManagerTest, Distributed_Update_00200, Function | SmallTest | Level1)
{
    sptr<NotificationRequest> request = new NotificationRequest(1000);
    request->SetLabel("<label>");

    std::string bundleName = "<bundleName>";
    std::string label = request->GetLabel();
    int32_t id = request->GetNotificationId();

    EXPECT_EQ(distributedManager_->Update(bundleName, label, id, request), ERR_ANS_DISTRIBUTED_OPERATION_FAILED);
}

/**
 * @tc.name      : Distributed_Delete_00100
 * @tc.number    : Distributed_Delete_00100
 * @tc.desc      : Delete a local notification to remote used distributed.
 */
HWTEST_F(DistributedNotificationManagerTest, Distributed_Delete_00100, Function | SmallTest | Level1)
{
    sptr<NotificationRequest> request = new NotificationRequest(1000);
    request->SetLabel("<label>");

    std::string bundleName = "<bundleName>";
    std::string label = request->GetLabel();
    int32_t id = request->GetNotificationId();

    EXPECT_EQ(distributedManager_->Delete(bundleName, label, id), ERR_ANS_DISTRIBUTED_OPERATION_FAILED);
}

/**
 * @tc.name      : Distributed_Delete_00200
 * @tc.number    : Distributed_Delete_00200
 * @tc.desc      : Delete a remote notification to remote used distributed.
 */
HWTEST_F(DistributedNotificationManagerTest, Distributed_Delete_00200, Function | SmallTest | Level1)
{
    sptr<NotificationRequest> request = new NotificationRequest(1000);
    request->SetLabel("<label>");

    std::string deviceId = "<remoteDeviceId>";
    std::string bundleName = "<bundleName>";
    std::string label = request->GetLabel();
    int32_t id = request->GetNotificationId();

    EXPECT_EQ(distributedManager_->DeleteRemoteNotification(deviceId, bundleName, label, id),
        ERR_ANS_DISTRIBUTED_OPERATION_FAILED);
}

/**
 * @tc.name      : Distributed_Delete_00300
 * @tc.number    : Distributed_Delete_00300
 * @tc.desc      : Delete a remote notification to remote used distributed.
 */
HWTEST_F(DistributedNotificationManagerTest, Distributed_Delete_00300, Function | SmallTest | Level1)
{
    sptr<NotificationRequest> request = new NotificationRequest(1000);
    request->SetLabel("<label>");

    std::string bundleName = "<bundleName>";
    std::string label = request->GetLabel();
    int32_t id = request->GetNotificationId();

    EXPECT_EQ(distributedManager_->Delete(bundleName, label, id), ERR_ANS_DISTRIBUTED_OPERATION_FAILED);
}

/**
 * @tc.name      : Distributed_Get_Current_Notification_00100
 * @tc.number    : Distributed_Get_Current_Notification_00100
 * @tc.desc      : Get current notification in distributed database.
 */
HWTEST_F(DistributedNotificationManagerTest, Distributed_Get_Current_Notification_00100, Function | SmallTest | Level1)
{
    std::vector<sptr<NotificationRequest>> requestList;
    EXPECT_EQ(distributedManager_->GetCurrentDistributedNotification(requestList),
        ERR_ANS_DISTRIBUTED_OPERATION_FAILED);
}

/**
 * @tc.name      : Distributed_Get_Local_DeviceInfo_00100
 * @tc.number    : Distributed_Get_Local_DeviceInfo_00100
 * @tc.desc      : Get local distributed device information.
 */
HWTEST_F(DistributedNotificationManagerTest, Distributed_Get_Local_DeviceInfo_00100, Function | SmallTest | Level1)
{
    DistributedDatabase::DeviceInfo deviceInfo;
    EXPECT_EQ(distributedManager_->GetLocalDeviceInfo(deviceInfo), ERR_OK);
}

/**
 * @tc.name      : Distributed_ResolveDistributedKey_00100
 * @tc.number    : Distributed_ResolveDistributedKey_00100
 * @tc.desc      : text ResolveDistributedKey function.
 */
HWTEST_F(DistributedNotificationManagerTest, Distributed_ResolveDistributedKey_00100, Function | SmallTest | Level1)
{
    std::string key("<key>");
    DistributedNotificationManager::ResolveKey resolveKey;
    EXPECT_EQ(distributedManager_->ResolveDistributedKey(key, resolveKey), false);
}

/**
 * @tc.name      : Distributed_ResolveDistributedKey_00200
 * @tc.number    : Distributed_ResolveDistributedKey_00200
 * @tc.desc      : text ResolveDistributedKey function.
 */
HWTEST_F(DistributedNotificationManagerTest, Distributed_ResolveDistributedKey_00200, Function | SmallTest | Level1)
{
    std::string key("deviceId|bundleName");
    DistributedNotificationManager::ResolveKey resolveKey;
    EXPECT_EQ(distributedManager_->ResolveDistributedKey(key, resolveKey), false);
}

/**
 * @tc.name      : Distributed_ResolveDistributedKey_00300
 * @tc.number    : Distributed_ResolveDistributedKey_00300
 * @tc.desc      : text ResolveDistributedKey function.
 */
HWTEST_F(DistributedNotificationManagerTest, Distributed_ResolveDistributedKey_00300, Function | SmallTest | Level1)
{
    std::string key("deviceId|bundleName|label");
    DistributedNotificationManager::ResolveKey resolveKey;
    EXPECT_EQ(distributedManager_->ResolveDistributedKey(key, resolveKey), false);
}

/**
 * @tc.name      : Distributed_ResolveDistributedKey_00400
 * @tc.number    : Distributed_ResolveDistributedKey_00400
 * @tc.desc      : text ResolveDistributedKey function.
 */
HWTEST_F(DistributedNotificationManagerTest, Distributed_ResolveDistributedKey_00400, Function | SmallTest | Level1)
{
    std::string key("deviceId|bundleName|label|0");
    DistributedNotificationManager::ResolveKey resolveKey;
    EXPECT_EQ(distributedManager_->ResolveDistributedKey(key, resolveKey), true);
}

/**
 * @tc.name      : Distributed_CheckDeviceId_00100
 * @tc.number    : Distributed_CheckDeviceId_00100
 * @tc.desc      : text CheckDeviceId function.
 */
HWTEST_F(DistributedNotificationManagerTest, Distributed_CheckDeviceId_00100, Function | SmallTest | Level1)
{
    std::string deviceId = "<remoteDeviceId>";
    std::string key("<key>");
    std::string value("<value>");
    // text OnDatabaseInsert function.
    distributedManager_->OnDatabaseInsert(deviceId, key, value);
    // text OnDatabaseUpdate function.
    distributedManager_->OnDatabaseUpdate(deviceId, key, value);
    // text OnDatabaseDelete function.
    distributedManager_->OnDatabaseDelete(deviceId, key, value);
    // text CheckDeviceId function.
    EXPECT_EQ(distributedManager_->CheckDeviceId(deviceId, key), false);
}

/**
 * @tc.name      : Distributed_CheckDeviceId_00200
 * @tc.number    : Distributed_CheckDeviceId_00200
 * @tc.desc      : text CheckDeviceId function.
 */
HWTEST_F(DistributedNotificationManagerTest, Distributed_CheckDeviceId_00200, Function | SmallTest | Level1)
{
    std::string deviceId = "deviceId";
    std::string key("deviceId|bundleName|label|0");
    std::string value("");
    // text OnDatabaseInsert function.
    distributedManager_->OnDatabaseInsert(deviceId, key, value);
    // text OnDatabaseUpdate function.
    distributedManager_->OnDatabaseUpdate(deviceId, key, value);
    // text OnDatabaseDelete function.
    distributedManager_->OnDatabaseDelete(deviceId, key, value);
    // text CheckDeviceId function.
    EXPECT_EQ(distributedManager_->CheckDeviceId(deviceId, key), true);
}

/**
 * @tc.name      : Distributed_OnDeviceDisconnected_00100
 * @tc.number    : Distributed_OnDeviceDisconnected_00100
 * @tc.desc      : text OnDeviceDisconnected function.
 */
HWTEST_F(DistributedNotificationManagerTest, Distributed_OnDeviceDisconnected_00100, Function | SmallTest | Level1)
{
    const std::string deviceId = "<remoteDeviceId>";
    // text OnDeviceDisconnected function.
    distributedManager_->OnDeviceDisconnected(deviceId);
    // text PublishCallback function.
    const std::string bundleName = "<bundleName>";
    sptr<NotificationRequest> request = new NotificationRequest(1);
    EXPECT_EQ(distributedManager_->PublishCallback(deviceId, bundleName, request), true);
}

/**
 * @tc.name      : Distributed_UpdateCallback_00100
 * @tc.number    : Distributed_UpdateCallback_00100
 * @tc.desc      : text UpdateCallback function.
 */
HWTEST_F(DistributedNotificationManagerTest, Distributed_UpdateCallback_00100, Function | SmallTest | Level1)
{
    std::string deviceId = "<remoteDeviceId>";
    std::string bundleName = "<bundleName>";
    sptr<NotificationRequest> request = new NotificationRequest(1000);
    EXPECT_EQ(distributedManager_->UpdateCallback(deviceId, bundleName, request), true);
}

/**
 * @tc.name      : Distributed_DeleteCallback_00100
 * @tc.number    : Distributed_DeleteCallback_00100
 * @tc.desc      : text DeleteCallback function.
 */
HWTEST_F(DistributedNotificationManagerTest, Distributed_DeleteCallback_00100, Function | SmallTest | Level1)
{
    sptr<NotificationRequest> request = new NotificationRequest(1000);
    request->SetLabel("<label>");

    std::string deviceId = "<remoteDeviceId>";
    std::string bundleName = "<bundleName>";
    std::string label = request->GetLabel();
    int32_t id = request->GetNotificationId();
    EXPECT_EQ(distributedManager_->DeleteCallback(deviceId, bundleName, label, id), true);
}

/**
 * @tc.name      : Distributed_OnDistributedKvStoreDeathRecipient_00100
 * @tc.number    : Distributed_OnDistributedKvStoreDeathRecipient_00100
 * @tc.desc      : text OnDistributedKvStoreDeathRecipient function.
 */
HWTEST_F(DistributedNotificationManagerTest, Distributed_OnDistributedKvStoreDeathRecipient_00100,
         Function | SmallTest | Level1)
{
    EXPECT_EQ(distributedManager_->OnDistributedKvStoreDeathRecipient(), ERR_OK);
}

/**
 * @tc.number    : CheckDistributedNotificationType_0100
 * @tc.name      : CheckDistributedNotificationType_0100
 * @tc.desc      : Test CheckDistributedNotificationType function
 */
HWTEST_F(DistributedNotificationManagerTest, CheckDistributedNotificationType_0100, Function | SmallTest | Level1)
{
    GTEST_LOG_(INFO) << "CheckDistributedNotificationType_0100 test start";

    sptr<NotificationRequest> req = new NotificationRequest();
    EXPECT_EQ(distributedManager_->CheckDistributedNotificationType(req), true);

    GTEST_LOG_(INFO) << "CheckDistributedNotificationType_0100 test end";
}

/**
 * @tc.number    : CheckDistributedNotificationType_0200
 * @tc.name      : CheckDistributedNotificationType_0200
 * @tc.desc      : Test CheckDistributedNotificationType function
 */
HWTEST_F(DistributedNotificationManagerTest, CheckDistributedNotificationType_0200, Function | SmallTest | Level1)
{
    GTEST_LOG_(INFO) << "CheckDistributedNotificationType_0200 test start";

    sptr<NotificationRequest> req = new NotificationRequest();
    std::vector<std::string> devices;
    devices.push_back("a");
    devices.push_back("b");
    devices.push_back("c");
    req->GetNotificationDistributedOptions().SetDevicesSupportDisplay(devices);
    EXPECT_EQ(distributedManager_->CheckDistributedNotificationType(req), true);

    GTEST_LOG_(INFO) << "CheckDistributedNotificationType_0200 test end";
}

/**
 * @tc.number    : CheckPublishWithoutApp_0100
 * @tc.name      : CheckPublishWithoutApp_0100
 * @tc.desc      : Test CheckPublishWithoutApp function
 */
HWTEST_F(DistributedNotificationManagerTest, CheckPublishWithoutApp_0100, Function | SmallTest | Level1)
{
    GTEST_LOG_(INFO) << "CheckPublishWithoutApp_0100 test start";

    int32_t userId = 1;
    sptr<NotificationRequest> request = new NotificationRequest();
    EXPECT_EQ(distributedManager_->CheckPublishWithoutApp(userId, request), false);

    GTEST_LOG_(INFO) << "CheckPublishWithoutApp_0100 test end";
}

/**
 * @tc.number    : CheckPublishWithoutApp_0200
 * @tc.name      : CheckPublishWithoutApp_0200
 * @tc.desc      : Test CheckPublishWithoutApp function
 */
HWTEST_F(DistributedNotificationManagerTest, CheckPublishWithoutApp_0200, Function | SmallTest | Level1)
{
    GTEST_LOG_(INFO) << "CheckPublishWithoutApp_0200 test start";

    int32_t userId = SYSTEM_APP_UID;
    sptr<NotificationRequest> request = new NotificationRequest();
    EXPECT_EQ(distributedManager_->CheckPublishWithoutApp(userId, request), false);

    GTEST_LOG_(INFO) << "CheckPublishWithoutApp_0200 test end";
}
}  // namespace Notification
}  // namespace OHOS