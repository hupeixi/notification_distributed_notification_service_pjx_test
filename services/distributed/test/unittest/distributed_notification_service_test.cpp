/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <memory>

#include "gtest/gtest.h"

#define private public
#include "distributed_notification_service.h"

using namespace testing::ext;
namespace OHOS {
namespace Notification {
class DistributedNotificationServiceTest : public testing::Test {
public:
    void SetUp() override;
    void TearDown() override;
};

void DistributedNotificationServiceTest::SetUp()
{}

void DistributedNotificationServiceTest::TearDown()
{}

/**
 * @tc.name      : GetInstance_00100
 * @tc.number    : GetInstance_00100
 * @tc.desc      : test GetInstance function .
 */
HWTEST_F(DistributedNotificationServiceTest, GetInstance_00100, Function | SmallTest | Level1)
{
    sptr<DistributedNotificationService> instance = DistributedNotificationService::GetInstance();
    ASSERT_NE(nullptr, instance);
}

/**
 * @tc.name      : OnDistributedKvStoreDeathRecipient_00100
 * @tc.number    : OnDistributedKvStoreDeathRecipient_00100
 * @tc.desc      : test OnDistributedKvStoreDeathRecipient function .
 */
HWTEST_F(DistributedNotificationServiceTest, OnDistributedKvStoreDeathRecipient_00100, Function | SmallTest | Level1)
{
    sptr<DistributedNotificationService> instance = DistributedNotificationService::GetInstance();
    ASSERT_NE(nullptr, instance);
    ErrCode result = instance->OnDistributedKvStoreDeathRecipient();
    EXPECT_EQ(result, ERR_OK);
}
 
/**
 * @tc.name      : Delete_00100
 * @tc.number    : Delete_00100
 * @tc.desc      : test Delete function  .
 */
HWTEST_F(DistributedNotificationServiceTest, Delete_00100, Function | SmallTest | Level1)
{
    sptr<DistributedNotificationService> instance = DistributedNotificationService::GetInstance();
    ASSERT_NE(nullptr, instance);

    int32_t id = 0;
    std::string label = "<label>";
    std::string bundleName = "<bundleName>";
    ErrCode result = instance->Delete(bundleName, label, id);
    EXPECT_EQ(result, ERR_OK);
}

/**
 * @tc.name      : DeleteRemoteNotification_00100
 * @tc.number    : DeleteRemoteNotification_00100
 * @tc.desc      : test DeleteRemoteNotification function  .
 */
HWTEST_F(DistributedNotificationServiceTest, DeleteRemoteNotification_00100, Function | SmallTest | Level1)
{
    sptr<DistributedNotificationService> instance = DistributedNotificationService::GetInstance();
    ASSERT_NE(nullptr, instance);

    int32_t id = 0;
    std::string label = "<label>";
    std::string deviceId = "<deviceId>";
    std::string bundleName = "<bundleName>";
    ErrCode result = instance->DeleteRemoteNotification(deviceId, bundleName, label, id);
    EXPECT_EQ(result, ERR_OK);
}

/**
 * @tc.name      : Publish_00100
 * @tc.number    : Publish_00100
 * @tc.desc      : test Publish function  .
 */
HWTEST_F(DistributedNotificationServiceTest, Publish_00100, Function | SmallTest | Level1)
{
    sptr<DistributedNotificationService> instance = DistributedNotificationService::GetInstance();
    ASSERT_NE(nullptr, instance);

    int32_t id = 0;
    std::string label = "<label>";
    std::string deviceId = "<deviceId>";
    std::string bundleName = "<bundleName>";
    sptr<OHOS::Notification::NotificationRequest> request =
        new (std::nothrow) OHOS::Notification::NotificationRequest();
    ASSERT_NE(nullptr, request);
    ErrCode result = instance->Publish(bundleName, label, id, request);
    EXPECT_EQ(result, ERR_OK);
}

/**
 * @tc.name      : CheckRemoteDevicesIsUsing_00100
 * @tc.number    : CheckRemoteDevicesIsUsing_00100
 * @tc.desc      : test CheckRemoteDevicesIsUsing function  .
 */
HWTEST_F(DistributedNotificationServiceTest, CheckRemoteDevicesIsUsing_00100, Function | SmallTest | Level1)
{
    sptr<DistributedNotificationService> instance = DistributedNotificationService::GetInstance();
    ASSERT_NE(nullptr, instance);

    bool isUsing = false;
    ErrCode result = instance->CheckRemoteDevicesIsUsing(isUsing);
    EXPECT_EQ(result, ERR_OK);
}

/**
 * @tc.name      : SetLocalScreenStatus_00100
 * @tc.number    : SetLocalScreenStatus_00100
 * @tc.desc      : test SetLocalScreenStatus function  .
 */
HWTEST_F(DistributedNotificationServiceTest, SetLocalScreenStatus_00100, Function | SmallTest | Level1)
{
    sptr<DistributedNotificationService> instance = DistributedNotificationService::GetInstance();
    ASSERT_NE(nullptr, instance);

    bool screenOn = false;
    ErrCode result = instance->SetLocalScreenStatus(screenOn);
    EXPECT_EQ(result, ERR_OK);
}

/**
 * @tc.name      : ResetFfrtQueue_00100
 * @tc.number    : ResetFfrtQueue_00100
 * @tc.desc      : test SetLocalScreenStatus function  .
 */
HWTEST_F(DistributedNotificationServiceTest, ResetFfrtQueue_00100, Function | SmallTest | Level1)
{
    sptr<DistributedNotificationService> instance = DistributedNotificationService::GetInstance();
    ASSERT_NE(nullptr, instance);

    bool screenOn = false;
    ErrCode result = instance->ResetFfrtQueue();
    EXPECT_EQ(result, ERR_OK);
}

}  // namespace Notification
}  // namespace OHOS