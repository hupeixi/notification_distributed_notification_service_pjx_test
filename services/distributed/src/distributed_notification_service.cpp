/*
 * Copyright (c) 2021-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "distributed_notification_service.h"

#include "ans_log_wrapper.h"

#include "ans_const_define.h"
#include "ans_inner_errors.h"
#include "os_account_manager.h"
#include "distributed_preferences.h"
#include "distributed_notification_manager.h"
#include "distributed_screen_status_manager.h"
#include "ipc_skeleton.h"
#include "ans_permission_def.h"
#include "errors.h"


namespace OHOS {
namespace Notification {

sptr<DistributedNotificationService> DistributedNotificationService::instance_;
std::mutex DistributedNotificationService::instanceMutex_;

sptr<DistributedNotificationService> DistributedNotificationService::GetInstance()
{
    std::lock_guard<std::mutex> lock(instanceMutex_);

    if (instance_ == nullptr) {
        instance_ = new (std::nothrow) DistributedNotificationService();
        if (instance_ == nullptr) {
            ANS_LOGE("Failed to create DistributedNotificationService instance");
            return nullptr;
        }
    }
    return instance_;
}

DistributedNotificationService::DistributedNotificationService()
{
    ANS_LOGI("constructor");
}

DistributedNotificationService::~DistributedNotificationService()
{
    ANS_LOGI("deconstructor");
}

ErrCode DistributedNotificationService::OnDistributedKvStoreDeathRecipient()
{
    ErrCode result = ERR_OK;
    auto distributedInstance = DistributedNotificationManager::GetInstance();
    if( distributedInstance == nullptr ) {
        return ERR_INVALID_STATE;
    }
    distributedInstance->OnDistributedKvStoreDeathRecipient();

    return result;
}

ErrCode DistributedNotificationService::Delete(const std::string &bundleName, const std::string &label, int32_t id) 
{
    auto distributedInstance = DistributedNotificationManager::GetInstance();
    if( distributedInstance == nullptr ) {
        return ERR_INVALID_STATE;
    }
    return distributedInstance->Delete(bundleName, label, id);  
}

ErrCode DistributedNotificationService::DeleteRemoteNotification(const std::string &deviceId,
    const std::string &bundleName, const std::string &label, int32_t id)
{
    auto distributedInstance = DistributedNotificationManager::GetInstance();
    if( distributedInstance == nullptr ) {
        return ERR_INVALID_STATE;
    }
    return distributedInstance->DeleteRemoteNotification(deviceId ,bundleName, label, id);  
}
    
ErrCode DistributedNotificationService::Publish(const std::string &bundleName, 
    const std::string &label, int32_t id, const sptr<NotificationRequest> &request)
{
    auto distributedInstance = DistributedNotificationManager::GetInstance();
    if( distributedInstance == nullptr ) {
        return ERR_INVALID_STATE;
    }
    return distributedInstance->Publish(bundleName, label, id, request);
}

ErrCode DistributedNotificationService::CheckRemoteDevicesIsUsing(bool &isUsing)
{
    auto distributedInstance = DistributedScreenStatusManager::GetInstance();
    if( distributedInstance == nullptr ) {
        return ERR_INVALID_STATE;
    }
    return distributedInstance->CheckRemoteDevicesIsUsing(isUsing);
}

ErrCode DistributedNotificationService::SetLocalScreenStatus(bool screenOn)
{
    auto distributedInstance = DistributedScreenStatusManager::GetInstance();
    if( distributedInstance == nullptr ) {
        return ERR_INVALID_STATE;
    }
    return distributedInstance->SetLocalScreenStatus(screenOn);
}

ErrCode DistributedNotificationService::ResetFfrtQueue()
{
    auto distributedInstance = DistributedNotificationManager::GetInstance();
    if( distributedInstance == nullptr ) {
        return ERR_INVALID_STATE;
    }
    distributedInstance->ResetFfrtQueue();
    return ERR_OK;
}

}  // namespace Notification
}  // namespace OHOS
